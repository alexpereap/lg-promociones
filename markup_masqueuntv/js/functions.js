// JavaScript Document

$(document).ready(function () {

    $(".ui-loader").hide();

	//tooltip
   $("[rel=tooltip]").tooltip({html:true});

	//Date picker
	$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		language: 'es'
	});

	//Lightbox prizes
	$('.imgPrize').click(function(){
		$('.prizeImg').empty();
		$($(this).parents('div').html()).appendTo('.prizeImg');
		$('#modalPrize').modal({show:true});
	});

	//Carousel mobile swipe
   $("#myCarousel").swiperight(function() {
      $(this).carousel('prev');
    });
   $("#myCarousel").swipeleft(function() {
      $(this).carousel('next');
   });

});