
## Al asosicar un codigo a un usuario se deshabilita el codigo

DELIMITER |
	CREATE TRIGGER disable_code AFTER INSERT ON tbl_user_codes
	FOR EACH ROW BEGIN

	UPDATE tbl_codes SET available = 'no' WHERE id = NEW.fk_codes_id;

	END
	|
	DELIMITER ;

## Al desasociar un codigo de un un usuario se habilita el codigo

DELIMITER |
	CREATE TRIGGER enable_code AFTER DELETE ON tbl_user_codes
	FOR EACH ROW BEGIN

	UPDATE tbl_codes SET available = 'si' WHERE id = OLD.fk_codes_id;

	END
	|
	DELIMITER ;


	DELIMITER |
	CREATE TRIGGER enable_code AFTER DELETE ON tbl_user_codes
	FOR EACH ROW BEGIN

	UPDATE tbl_codes SET available = 'si' WHERE id = OLD.fk_codes_id;

	END
	|
	DELIMITER ;


#nueva solicitud de beneficio

DELIMITER |
	CREATE TRIGGER log_new_request AFTER INSERT ON tbl_prize_application
	FOR EACH ROW BEGIN

	INSERT INTO tbl_prize_application_historic SET

	fk_prize_application_id = NEW.id,
	status = NEW.status,
	date = NOW();

	END
	|
	DELIMITER ;

#actualizacion en solicitud de beneficio

DELIMITER |
	CREATE TRIGGER log_update_request AFTER UPDATE ON tbl_prize_application
	FOR EACH ROW BEGIN

	INSERT INTO tbl_prize_application_historic SET

	fk_prize_application_id = NEW.id,
	status = NEW.status,
	date = NOW();

	END
	|
	DELIMITER ;