DROP VIEW vw_prize_beneficts;

CREATE VIEW `vw_prize_beneficts` AS
    select
        `p`.`id` AS `id`,
        `p`.`fk_tv_id` AS `fk_tv_id`,
        `t`.`reference` AS `reference`,
        `p`.`title` AS `title`,
        `p`.`image` AS `image`,
        `p`.`description` AS `description`,
        `p`.`created_at` AS `created_at`,
        `p`.`quanty` AS `quanty`,
        `p`.`terms` AS `terms`,
        `p`.`has_promo_code` AS `has_promo_code`,
        group_concat(`pd`.`title`
            separator ', ') AS `beneficts`
    from
        (((`tbl_prize` `p`
        join `tbl_prize_elements` `pe` ON ((`pe`.`fk_prize_id` = `p`.`id`)))
        join `tbl_prize_detail` `pd` ON ((`pd`.`id` = `pe`.`fk_prize_detail_id`)))
        join `tbl_tv` `t` ON ((`p`.`fk_tv_id` = `t`.`id`)))
    group by `p`.`id`;


DROP VIEW vw_available_tv_prizes;

CREATE VIEW `vw_available_tv_prizes` AS
    select
        `t`.`id` AS `tv_id`,
        `t`.`reference` AS `reference`,
        `p`.`id` AS `prize_id`,
        `p`.`title` AS `prize_title`,
        `p`.`terms` AS `prize_terms`,
        `p`.`image` AS `prize_image`,
        `p`.`description` AS `prize_description`,
        `p`.`has_promo_code` AS `prize_has_promo_code`,
        `p`.`quanty` AS `prize_quanty`
    from
        (`tbl_prize` `p`
        join `tbl_tv` `t` ON ((`t`.`id` = `p`.`fk_tv_id`)));