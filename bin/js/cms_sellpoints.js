$(document).ready(function(){

    $(document).on("click", ".crud-action", function(){
        if( $(this).attr('title').toLowerCase().trim() == 'eliminar' ){

            s = $(this).attr('href').trim().split("/");
            sellpoint_id = s[s.length-1];

            if( confirm("¿ Estas seguro de eliminar este punto de venta ?") ){

                deleteSellpoint(sellpoint_id);
            }


            return false;
        }
    });
});


function deleteSellpoint(sellpoint_id){

    $.ajax({
        url : 'cms/delete_sellpoint',
        type: 'post',
        dataType : 'json',
        data:({
            sellpoint_id : sellpoint_id
        }),
        success:function(data){
            if( data.result ){
                window.location.reload();
            } else {
                alert("Hubo un problema procesado tu solicitud, intentalo de nuevo");
            }
        }
    });
}