var controllers = {};
var cmsApp = angular.module('cmsApp', [] );


$(document).ready(function(){

    $("#purchaseAmount").keydown(function(e){

        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#purchaseAmount").keyup(function(){
        if( $(this).val() != '' ){
            $(this).val(  number_format( $(this).val() ) );
        }
    });

    // try - catching since jquery ui isn't available in every cms view
    try{
        $("#promotor").autocomplete({
            source: 'site/get_promotor',
            minLength : 2,
            select : function(event, ui){

                $("#promotor_id").val(ui.item.id);

            },
            change: function(event, ui) {
                if( ui.item == null ){

                    $("#promotor_id, #promotorQry").val('');

                }
            }
        });
    } catch( err ){
        // console.log(err.message);
    }

    $("#cancelBtn, .cancel-btn").click(function(){

        if( confirm("Deseas cancelar el envío del fomrulario") ){
            window.location.href = $(this).attr('route');
        }
    });

    $('.cms-datepicker').datepicker({
        format: "yyyy-mm-dd",
        language: 'es'
    });

    $(".make-fancy-multiselect").multiSelect();

    $("#sellpointsList").jstree({
        "plugins" : [ "checkbox" ]
    });


    $("#prizePackageForm").submit(function(){

        sell_point_ids = $("#sellpointsList").jstree("get_bottom_checked");

        /*if( sell_point_ids.length < 1 ){
            alert("Selecciona almenos un punto de venta");
            return false;
        }*/

        $("#sellpointIds").val( implode(',', sell_point_ids) );

    });

    if( $("#prizeList").length > 0 ){
        $("a[title=Eliminar]").click(function(){

            return confirm("Estas seguro que quieres eliminar este registro?");

        });
    }

    $(document).on("click", "a.crud-action[title='Eliminar Solicitud']", function(){

        return confirm("Estas seguro de eliminar esta Solicitud?\nEsta acción no se puede deshacer.");

    });

    $(document).on("click", "a.crud-action[title='Reenviar beneficios']", function(e){

        var accept_request = false;

        // gets prize application id
        url = $(this).attr('href').split('/');
        var prize_application_id = parseInt(url[ url.length - 1 ]);

        // gets some basic info on prize app id
        $.ajax({
            url      :'cms/ajax_get_prize_req_data',
            type     :'post',
            dataType :'json',
            async    : false,
            data:({
                prize_application_id : prize_application_id
            }),
            success:function(result){

                if( result.status == 'aprobado' || result.status == 'entregado' ){

                    accept_request = confirm( "Se reenviarán los beneficios otorgados al email " + result.email + ".\nNo se usarán nuevos códigos ni se restará la cantidad de beneficios dispobnibles.\nSe enviará un email de copia con los beneficios al correo de moderación." );

                } else {
                    alert("Esta solicitud no se encuentra aprobada, no se pueden reenviar los beneficios.");
                }
            }

        });

        if( !accept_request ){

            e.preventDefault();
            return false;
        }

    });


    $("#main-table-box").bind("DOMSubtreeModified",  function(){
        $(".admin-edit-request").attr('target','_blank');
    });


});


$(window).load(function(){

    $(".admin-edit-request").attr('target','_blank');
    if( $("#inPrizeDetail").length > 0 ){

        if( typeof $("select[name='has_list_of_codes'] option:selected").val() != 'undefined' ){

            // add mode avaialble quanty field handler
            $("select[name='has_list_of_codes']").change(function(){

                switch( $(this).val() ){

                    case 'yes':
                        $("#available_quanty_field_box").hide();
                        break;

                    case 'no':
                        default:
                        $("#field-available_quanty").val('');
                        $("#available_quanty_field_box").show();
                        break;

                }
            });

            // edit mode inital state of available quanty field
            switch( $("select[name='has_list_of_codes'] option:selected").val() ){

                case 'yes':
                default:
                    $("#available_quanty_field_box").hide();
                    break;

                case 'no':
                    $("#available_quanty_field_box").show();
                    break;

            }
        }

    }

});



controllers.sellPointController =
    function sellPointController($scope, $http){

        $scope.cityStoresSwitch = true;
        $scope.cityStores = null;
        $scope.cityStoresEmptyValue = "Selecciona una ciudad";
        $scope.saveDisabled = false;

        $scope.updateSellPoint = function(city_id){
            if( city_id != '' ){

                $scope.cityStoresSwitch = true;
                $scope.saveDisabled = true;

                 $http.get("cms/get_city_stores?city_id="+city_id)
                .success(function(response) {

                    $scope.cityStoresSwitch = false;
                    $scope.saveDisabled = false;


                    if( response.success  ){
                        $scope.cityStores = response.data;
                        $scope.cityStoresSwitch = false;
                        $scope.cityStoresEmptyValue = "Selecciona una cadena";
                    } else {
                        $scope.cityStoresSwitch = true;
                        $scope.cityStores = null;
                        $scope.cityStoresEmptyValue = "Selecciona una ciudad";
                    }

                });
            } else {

                $scope.cityStoresSwitch = true;
                $scope.cityStores = null;
                $scope.cityStoresEmptyValue = "Selecciona una ciudad";
            }
        }

    };
/*control para clonar varias veces el mismo elemento*/
controllers.prizesController =
    function prizesController($scope, $http){

        $scope.selected_values = null;

        $scope.store_qty = 1;
        $scope.store_blocks = [ { id: 1 } ];

        $scope.cityStoresSwitch = new Array;
        $scope.cityStores = new Array;
        $scope.cityStoresEmptyValue = new Array;

        $scope.addStoreBlock = function(){
            current_id = ++$scope.store_qty;
            $scope.store_blocks.push( {id:current_id  } );

            $scope.cityStoresSwitch[ current_id ] = true;
            $scope.cityStoresEmptyValue[ current_id ] = "Selecciona una ciudad";
        }


        $scope.cityStoresSwitch[1] = true;
        $scope.cityStoresEmptyValue[1] = "Selecciona una ciudad";

        $scope.updateSellPoint = function(city_id, store_block_id){
            if(  city_id != undefined  && city_id.trim() != '' ){


                $scope.cityStoresSwitch[store_block_id] = true;

                 $http.get("cms/get_city_stores?city_id="+city_id)
                .success(function(response) {

                    $scope.cityStoresSwitch[store_block_id] = false;

                    if( response.success  ){
                        $scope.cityStores[store_block_id] = response.data;
                        $scope.cityStoresSwitch[store_block_id] = false;
                        $scope.cityStoresEmptyValue[store_block_id] = "Selecciona una cadena";
                    } else {
                        $scope.cityStoresSwitch[store_block_id] = true;
                        $scope.cityStores[store_block_id] = null;
                        $scope.cityStoresEmptyValue[store_block_id] = "Selecciona una ciudad";
                    }

                });
            } else {

                $scope.cityStoresSwitch[store_block_id] = true;
                $scope.cityStores[store_block_id] = null;
                $scope.cityStoresEmptyValue[store_block_id] = "Selecciona una ciudad";
            }

        }

        // for testing purposes
        $scope.checkValues = function(){
            console.log($scope.store_qty);
            console.log($scope.store_blocks);
            console.log($scope.cityStoresSwitch);
            console.log($scope.cityStores);
            console.log($scope.cityStoresEmptyValue);
        }
};



// Register App controllers
cmsApp.controller( controllers );
