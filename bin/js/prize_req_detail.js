$(document).ready(function(){

    $("#request_status").change(function(){

        if( $(this).val() == 'aprobado' ){

            $("#sendApprovalMail").prop('checked',true);
            $("#reqNotimailWrapper").slideDown();

        } else {

            $("#sendApprovalMail").prop('checked',false);
            $("#reqNotimailWrapper").slideUp();
        }


        if( $(this).val() == 'no_aprobado' ){

            $("#sendDenialMail").prop('checked',true);
            $("#reqNotimailWrapper2").slideDown();

        } else {

            $("#sendDenialMail").prop('checked',false);
            $("#reqNotimailWrapper2").slideUp();
        }
    });
});