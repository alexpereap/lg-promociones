var controllers = {};
var siteApp = angular.module('siteApp', [] );

// online case IDS
var onlineCityId = 205;
var onlineStoreId = 268;
var onlineSellpointID = 515;

$(document).ready(function(){

    $("#purchaseAmount").keydown(function(e){

        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#purchaseAmount").keyup(function(){
        if( $(this).val() != '' ){
            $(this).val(  number_format( $(this).val() ) );
        }
    });


    $("#loginForm").bootstrapValidator();

        /*validate only numbers*/
          $('#celPhone, #phone').keyup(function (){
            this.value = (this.value + '').replace(/[^0-9]/g, '');
          });

          $('#name').keyup(function (){
            this.value = (this.value + '').replace(/[^a-zA-Z ñáéíóú ÑÁÉÍÓÚ]/g, '');
          });

           $('#lastName').keyup(function (){
            this.value = (this.value + '').replace(/[^a-zA-Z ñáéíóú ÑÁÉÍÓÚ]/g, '');
          });


         // opens bills modal
         if( $("#openBillsModal").val() == 'yes' ){
            $("#triggermodalReceipt").click();
         }

        $(document).on("click", '.linkTerms', function(){
            ga_data = $(this).attr('data-marcacion').split(',');
            ga( ga_data[0], ga_data[1], ga_data[2], ga_data[3], ga_data[4] );
         });

        $(document).on("click", '.link-view-benefict', function(){
            ga_data = $(this).attr('data-marcacion').split(',');
            ga( ga_data[0], ga_data[1], ga_data[2], ga_data[3], ga_data[4] );
         });

        $(document).on("click", '.link-delete-benefict', function(){
            ga_data = $(this).attr('data-marcacion').split(',');
            ga( ga_data[0], ga_data[1], ga_data[2], ga_data[3], ga_data[4] );
         });

        $("#updatePersonalDataBtn").click(function(){

            var allowedExtensions = ['jpg','jpeg','png','gif'];

            if( $("#nid_img_front").val() != '' ){


                ext = $("#nid_img_front").val().split('.');
                extension = ext[ ext.length -1 ].toLowerCase();

                if( !in_array( extension, allowedExtensions ) ){
                    alert('El archivo de la foto de cédula frontal no es válido, solo admitimos jpg, png, gif.');
                    return false;
                }


            }

            if( $("#nid_img_back").val() != '' ){

                ext = $("#nid_img_back").val().split('.');
                extension = ext[ ext.length -1 ].toLowerCase();

                if( !in_array( extension, allowedExtensions ) ){
                    alert('El archivo de la foto de cédula trasera no es válido, solo admitimos jpg, png, gif.');
                    return false;
                }
            }

            if( $("#name").val() == '' ){
                alert("El campo de nombre es requerido");
                return false;
            }

            if( $("#lastName").val() == '' ){
                alert("El campo de apellido es requerido");
                return false;
            }

            if( $("#celPhone").val() == '' ){
                alert("El campo celular es requerido");
                return false;
            }

            if( $("#email").val() == '' ){
                alert("El campo email es requerido");
                return false;
            }

            form = $("#registerForm");
            form.attr('action','site/update_personal_data');

            form.submit();

        });

});


controllers.prizeRequestController =
    function prizeRequestController($scope, $http, $sce){

        $scope.prizes = [];
        $scope.index = [];
        $scope.currentSerial = null;
        $scope.tvRef = null;
        $scope.currentTvData = null;

        // combobox data
        $scope.stores = null;
        $scope.sellpoints = null;

        // disables stores combobox
        $scope.disabledStores = true;
        $scope.disabledSellPoints = true;

        $scope.modalAlertMessage = 'Escoge una referencia de la lista';

        $scope.modalErrorAlertMessage = new Object;
        $scope.disabledSubmitBtn = false;

        $scope.enable_promo_code = false;

        $scope.phone_area_code = null;

        $scope.init = function(){
            $scope.prizes = angular_preload.prizes;
            $scope.stores = angular_preload.stores;
            $scope.sellpoints = angular_preload.sellpoints;
            $scope.disabledStores = angular_preload.disabledStores;
            $scope.disabledSellPoints = angular_preload.disabledSellPoints;

            // makes safe content
            for( x in $scope.prizes ){

                $scope.prizes[x].description = $sce.trustAsHtml( $scope.prizes[x].description );
                $scope.prizes[x].terms = $sce.trustAsHtml( $scope.prizes[x].terms );

                if( $scope.prizes[x].has_promo_code === true ){
                    $scope.enable_promo_code = true;
                }

            }
            $scope.currentCode = angular_preload.currentCode;
            $scope.selectedSellpointId = angular_preload.selectedSellpointId;
            $scope.phone_area_code = angular_preload_2.phone_area_code;
        }

        // init outside edit mode
        $scope.single_init = function(){
            $scope.phone_area_code = angular_preload_2.phone_area_code;

        }

        $scope.test = function(){

            console.log($scope.prizes);
            console.log($scope.stores);
            console.log($scope.sellpoints);
        }


        $scope.addTv = function(){

            console.log( $scope.currentTvData );

            if($scope.currentTvData != null ){

                // checks quanty availability
                /*if( $scope.currentTvData.quanty == 0 ){
                    $scope.modalAlertMessage = '¡Lo sentimos! Las existencias de este beneficio estan agotadas';
                    $('#modalAlert').modal();

                    return;
                }*/

                // checks date avialability
                /*if( $scope.currentTvData.date_availability === false ){
                    // $scope.modalAlertMessage = '¡Lo sentimos! la fecha para reclamar este beneficio ha caducado. (El beneficio era váido desde: ' + $scope.currentTvData.available_from + ' hasta: ' +$scope.currentTvData.available_until + ').';
                    $scope.modalAlertMessage = '¡Lo sentimos! La referencia de TV que ingresaste no cuenta actualmente con un beneficio.';
                    $('#modalAlert').modal();

                    return;
                }*/

                // checks quanty availability and date availability
                if( typeof $("#adminMode").val() == 'undefined' ){
                    switch( $scope.currentTvData.date_availability ){

                        // no beneficts available
                        case false:
                            $scope.modalAlertMessage = '¡Lo sentimos! La referencia de TV que ingresaste no cuenta actualmente con un beneficio.';
                            $('#modalAlert').modal();
                            return;
                            break;

                        // some beneficts available
                        case 'partial':
                            $scope.modalAlertMessage = $scope.currentTvData.unavailable_message;
                            $('#modalAlert').modal();
                            break;
                    }
                }

                if( typeof $("#adminMode").val() == 'undefined' ){
                    if( $scope.prizes.length >= 3 ){
                        $scope.modalAlertMessage = '¡Lo sentimos! No puedes agregar más de 3 productos por factura.';
                        $('#modalAlert').modal();

                        return;
                    }
                }

                if( $scope.currentSerial == null ){
                    $scope.modalAlertMessage = 'Especifica el serial';
                    $('#modalAlert').modal();

                    return;
                }

                // checks if whether to hide or unhide the promo code field
                if( $scope.currentTvData.has_promo_code === true ){
                    $scope.enable_promo_code = true;
                }

                // converts this data to safe html
                $scope.currentTvData.description = $sce.trustAsHtml( $scope.currentTvData.description );
                $scope.currentTvData.terms = $sce.trustAsHtml( $scope.currentTvData.terms );

                $scope.currentTvData.serial = $scope.currentSerial;

                $scope.currentTvData.marcacion_terms = "send,event, Formulario,Click, Términos-y-Condiciones_Promo" + ($scope.prizes.length +1) + "_Datos-de-tu-TV'";
                $scope.currentTvData.marcacion_ver = "send,event, Formulario,Click, Ver-promo_Promo" + ($scope.prizes.length +1) + "_Datos-de-tu-TV'";
                $scope.currentTvData.marcacion_trash = "send,event, Formulario,Click, Eliminar_Promo" + ($scope.prizes.length +1) + "_Datos-de-tu-TV'";


                $scope.prizes.push($scope.currentTvData);
                $scope.currentTvData = null;
                $scope.currentSerial = null;
                $scope.tvRef = null;


            }


        }

        $scope.removeTv = function (tv_id){

            enable_promo_code = false;

            for( x in $scope.prizes ){

                if( $scope.prizes[x].tv_id == tv_id ){
                    index = parseInt(x);
                    break;
                }
            }

            // removes tv
            if (index > -1) {
                $scope.prizes.splice(index, 1);
            }

            // checks if whether to hide or unhide the promo code field
            // if some prize has promo code option the prom code field wont be hide
            for( x in $scope.prizes ){
                if( $scope.prizes[x].has_promo_code === true ){
                    enable_promo_code = true;
                    break;
                }
            }

            $scope.enable_promo_code = enable_promo_code;
        }

        $scope.setStores = function(cityId){

            $scope.disabledStores = true;
            $scope.selectedSellpointId = null;

            // resets store and sellpoint data
            $scope.sellpoints = null;
            $scope.disabledSellPoints = true;

            $http.get("site/ajax_get_city_stores?city_id="+cityId)
                .success(function(response) {


                    if( response.length > 0 ){
                        $scope.stores = response;

                        // special case online store
                        if( cityId == onlineCityId ){
                            setTimeout(function(){
                                $("#storeSelect option").last().attr('selected','selected');
                                $scope.setSellpoints( onlineStoreId );
                            }, 100);
                        } else {
                            $("#code").removeAttr('disabled');
                        }

                        $scope.disabledStores = false;
                    } else {

                        $scope.stores = null;
                        $scope.selectedSellpointId = null;

                    }

                });
        }

        // sets local phone area code
        $scope.setAreaCode = function(cityId){

            $http.get("site/ajax_get_city_info?city_id=" + cityId).
                success(function(response){

                    try{

                        if( typeof response.area_code != 'undefined' && response.area_code != null ){
                            $scope.phone_area_code = '+' + response.area_code;
                        } else {
                            $scope.phone_area_code = null;
                        }
                    } catch( err ){
                        console.log(err.message);
                    }

                });
        }

        $scope.setSellpoints = function( storeId ){

            $scope.disabledSellPoints = true;
            $scope.selectedSellpointId = null;

            $http.get("site/ajax_get_store_sell_points?store_id="+storeId)
            .success(function(response) {


                if( response.length > 0 ){
                    $scope.sellpoints = response;

                    if( storeId == onlineStoreId ){
                        setTimeout(function(){
                            $("#sellpointSelect option").last().attr('selected','selected');
                            $scope.selectedSellpointId = onlineSellpointID;

                            $("#code").attr('disabled','disabled');

                            $("input[name=sell_point_id]").val( onlineSellpointID );

                        }, 110);
                    } else {
                        $("#code").removeAttr('disabled');
                    }

                    $scope.disabledSellPoints = false;
                } else {
                    $scope.sellpoints = null;
                    $scope.selectedSellpointId = null;
                }

            });
        }


        $scope.sendForm = function(){
            $("#registerForm").submit();
        }

        $scope.gaclick = function(a,b,c,d,e){

            console.log(a);
            //ga(a,b,c,d,e);
        }

        $("#registerForm").submit(function(){

            if( $("#registerForm").attr('action') == 'site/update_personal_data' ){
                return true;
            }


            var error_msg = '';
            var allowedExtensions = ['jpg','jpeg','png','gif'];

            if( !$("#checkHabeas").is(':checked') ){
                error_msg += '<li>Por favor acepta el tratamiento de datos.</li>';
            }

            if( $("#name").val() == '' ){
                error_msg += '<li>El campo nombre es obligatorio.</li>';
            }

            if( $("#lastName").val() == '' ){
                error_msg += '<li>El campo apellido es obligatorio.</li>';
            }

            if( typeof $("#adminMode").val() == 'undefined' ){
                if( $("#celPhone").val() == '' ){
                    error_msg += '<li>El campo celular es obligatorio.</li>';
                }
            }

            if( typeof $("#adminMode").val() == 'undefined' ){
                if( $("#registerForm").attr('validate-cc-img-front') == 'yes' ){
                    if( $("#registerForm").attr('edit-mode') != 'yes' )
                    if( $("#nid_img_front").val() == '' ){
                        error_msg += '<li>Hace falta la foto frontal de tu cédula.</li>';
                    } else{
                        ext = $("#nid_img_front").val().split('.');
                        extension = ext[ ext.length -1 ].toLowerCase();

                        if( !in_array( extension, allowedExtensions ) ){
                            error_msg += '<li>El archivo de la foto de cédula frontal no es válido, solo admitimos jpg, png, gif.</li>';
                        }
                    }
                }

                if( $("#registerForm").attr('validate-cc-img-back') == 'yes' ){
                    if( $("#registerForm").attr('edit-mode') != 'yes' )
                    if( $("#nid_img_back").val() == '' ){
                        error_msg += '<li>Hace falta la foto trasera de tu cédula.</li>';
                    } else {
                        ext = $("#nid_img_back").val().split('.');
                        extension = ext[ ext.length -1 ].toLowerCase();

                        if( !in_array( extension, allowedExtensions ) ){
                            error_msg += '<li>El archivo de la foto de cédula trasera no es válido, solo admitimos jpg, png, gif.</li>';
                        }
                    }
                }
            }

            if( $("#registerForm").attr('edit-mode') == 'yes' ){

                if( $("#nid").val() == '' ){
                    error_msg += '<li>No has especificado el número de cédula.</li>';
                }

                if( $("#email").val() == '' ){
                    error_msg += '<li>No has especificado el email.</li>';
                }

            }

            if( $scope.prizes.length == 0 ){
                error_msg += '<li>No has añadido ningún televisor.</li>';
            }

            if( $scope.selectedSellpointId == null || $scope.selectedSellpointId == ''  ){
                error_msg += '<li>No has seleccionado algún punto de venta.</li>';
            }

            if( $("#purchase_date").val() == '' ){
                error_msg += '<li>Especifica la fecha de compra.</li>';
            } else if( $scope.prizes.length > 0 && typeof $("#adminMode").val() == 'undefined' ) {

                current_date = new Date( $("#purchase_date").val() + ' 00:00' );
                date_availability_error = false;

                for( x in $scope.prizes ){

                    if( date_availability_error === true )
                        break;

                    for (y in $scope.prizes[x].beneficts_available_dates ){

                        available_from  = new Date($scope.prizes[x].beneficts_available_dates[y].available_from + ' 00:00' );
                        available_until = new Date($scope.prizes[x].beneficts_available_dates[y].available_until + ' 00:00' );

                        if( (available_from <= current_date && available_until >= current_date)  === false ){

                            error_msg += '<li>Lo sentiemos pero esta promoción ya no está vigente para la fecha de compra de su TV.</li>';
                            date_availability_error = true;
                            break;
                        }

                    }
                }
            }

            if( typeof $("#adminMode").val() == 'undefined' ){
                if( $("#purchaseAmount").val() == '' ){
                    error_msg += '<li>Especifica el precio de tu(s) tv LG</li>';
                }

                if( $("#registerForm").attr('edit-mode') != 'yes' )
                if( $("#bill_img").val() == '' ){
                    error_msg += '<li>Hace falta la foto de la factura.</li>';
                } else {
                    ext = $("#bill_img").val().split('.');
                    extension = ext[ ext.length -1 ].toLowerCase();

                    if( !in_array( extension, allowedExtensions ) ){
                        error_msg += '<li>El archivo de la foto de factura no es válido, solo admitimos jpg, png, gif.</li>';
                    }
                }
            }

            // an online buy does not require code
            if( $scope.selectedSellpointId  != onlineSellpointID){
                if( $("#code").val() == '' ){
                     error_msg += '<li>El campo código es obligatorio.</li>';
                }
            }

            // error_msg = '';

            if( error_msg != '' ){
                $scope.modalErrorAlertTitle   = 'Se encontraron los siguientes errores en tu solicitud:';
                $scope.modalErrorAlertMessage.message = '<ul>';
                $scope.modalErrorAlertMessage.message += error_msg;
                $scope.modalErrorAlertMessage.message += '</ul>';

                $scope.modalErrorAlertMessage.message = $sce.trustAsHtml($scope.modalErrorAlertMessage.message);
                $('#modalErrorAlert').modal();

                return false;
            } else {

                var validForm = true;
                $scope.disabledSubmitBtn = true;

                // validate terms y cons
                $(".checkTerms").each(function(){


                    if( !$(this).prop('checked') ){
                        $scope.modalAlertMessage = 'Debes aceptar los términos y condiciones de cada beneficio.';
                        $('#modalAlert').modal();

                        // kills form sending
                        validForm =  false;
                        $scope.disabledSubmitBtn = false;
                        return false;
                    }

                });

                // kill assoc with terms
                if( validForm === false ){
                    return false;
                }

                // validates nid number when creating a new request
                if( $("#validateNid").val() == 1 ){
                    validForm = verifyNid( $("#nid").val() );

                    // kill assoc with terms
                    if( validForm === false ){

                        $scope.modalAlertMessage = "El número de cédula " + $("#nid").val() + " ya existe. Inicia sesión con esta cédula en vez de generar un unevo registro con ella.";
                        $('#modalAlert').modal();
                        $scope.disabledSubmitBtn = false;

                        return false;
                    }
                }

                // an online buy does not require code
                if( $scope.selectedSellpointId  != onlineSellpointID){
                    $.ajax({
                        url : 'site/ajax_get_code',
                        async : false,
                        type : 'get',
                        dataType : 'json',
                        data :({
                            code : $scope.currentCode
                        }),
                        success:function(result){

                            if( result.success === true ){

                                if( $("#registerForm").attr('edit-mode') != 'yes' ){
                                    if( result.data.available != 'si' ){
                                        $scope.modalAlertMessage = 'El código '+ $scope.currentCode +' ya ha sido utilizado, por favor verifica.';
                                        $('#modalAlert').modal();
                                        $scope.disabledSubmitBtn = false;
                                        validForm = false;
                                    }
                                }

                            } else {
                                $scope.disabledSubmitBtn = false;
                                $scope.modalAlertMessage = 'El código '+ $scope.currentCode +' no es válido, por favor verifica que esté bien escrito.';
                                $('#modalAlert').modal();
                                validForm = false;
                            }
                        }
                    });
                }

                return validForm;
            }

        });

        $("#inputTvRef").autocomplete({
            source: 'site/get_available_prizes_tvs_byref',
            minLength : 2,
            select : function(event, ui){

                $scope.currentTvData = ui.item;

            },
            change: function(event, ui) {
                if( ui.item == null ){

                    $scope.modalAlertMessage = 'Escoge una referencia de la lista.';
                    $('#modalAlert').modal();


                    $scope.currentTvId = null;

                    $(this).val('');
                    $scope.currentSerial = null;
                    $(this).focus();
                }
            }
        });

        $("#promotorQry").autocomplete({
            source: 'site/get_promotor',
            minLength : 2,
            select : function(event, ui){

                $("#promotorId").val(ui.item.id);

            },
            change: function(event, ui) {
                if( ui.item == null ){

                    $("#promotorId, #promotorQry").val('');

                }
            }
        });

    };

// Register App controllers
siteApp.controller( controllers );

function verifyNid( nid ){

    var outcome = true;

    $.ajax({
        url   : 'site/ajax_verify_nid',
        type  : 'post',
        async : false,
        data  : ({
            nid : nid
        }),
        dataType : 'json',
        success:function(result){

            // if nid exists
            if( result.result === true ){
                outcome = false;
            }

        }
    });

   return outcome;
}