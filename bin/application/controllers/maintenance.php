<?php


class Maintenance extends CI_Controller{

    public function index(){

        $data = array(
            'message_title'     => 'Estamos realizando mantenimiento para darte un mejor servicio. Estaremos funcionando después de las 6:00 PM de hoy',
            'message_content'   => ''
            );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }
}

