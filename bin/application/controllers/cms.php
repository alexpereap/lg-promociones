<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller {

    private  $user_nid_imgs_folder = "uploads/nid_imgs";

    function __construct(){
        parent::__construct();

        /*if( $_SERVER['REMOTE_ADDR'] != '127.0.0.1' && $_SERVER['REMOTE_ADDR'] != '201.234.247.226' ){
            redirect('maintenance');
            return;
        }*/
        // krumo($this->session->all_userdata());
        // if  full request report  data doesn't exists create the default report data
        if( $this->session->userdata('full_request_report') === false ){
            $columns = array(
                'name'               => 1,
                'lastname'           => 1,
                'email'              => 1,
                'nid'                => 1,
                'prize_request_qty'  => 1,
                'city_name'          => 1,
                'tv_references'      => 1,
                'tv_references_list' => 1,
                'tv_serials_list'    => 1,
                'beneficts'          => 1,
                'used_codes'         => 1,
                'request_date'       => 1,
                'purchase_date'      => 1,
                'status'             => 1,
            );
            $this->session->set_userdata('full_request_report', $columns );
        }

        if( !$this->session->userdata('logged_in') &&  $this->router->method != 'login' && $this->router->method != 'check_login_credentials' ){

            redirect( 'cms/login' );
        }

        $this->load->library('grocery_CRUD');
    }

    public function index(){

        // test commit
        $this->load->view("cms/opener");
        $this->load->view("cms/closure");
    }

    // codes management
    public function codes(){

        $this->grocery_crud->set_table('tbl_codes');

        $action = $this->grocery_crud->getState();

        if( $action != 'edit' && $action != 'list' ){
            $this->grocery_crud->set_field_upload('value', 'assets/uploads/files/');
            $this->grocery_crud->callback_before_upload(array($this,'excel_validation'));
        }


        switch( $action ){
            case 'add':
                $this->grocery_crud->display_as('value', 'Archivo (excel xls, xlsx)');
                $this->grocery_crud->unset_fields('available');
                break;

            case 'edit':
                $this->grocery_crud->display_as('value', 'Código');
                break;

            default:
                $this->grocery_crud->display_as('value', 'Código');
                break;
        }

        $this->grocery_crud->display_as('available', 'Disponible');
        $this->grocery_crud->required_fields('value');

        $this->grocery_crud->callback_insert(array($this,'insert_custom_lg_codes'));

        $output = $this->grocery_crud->render();

        $output->in_codes = true;
        $output->in_codes_list = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/codes/codes");
        $this->load->view("cms/closure");
    }

    public function promo_code_txt(){
        $this->grocery_crud->set_table('tbl_txt_promocode');

        $this->grocery_crud->display_as('value','Texto');
        $this->grocery_crud->required_fields('value');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_delete();

        $output = $this->grocery_crud->render();

        $output->in_codes = true;
        $output->in_codes_text = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/codes/code_txt");
        $this->load->view("cms/closure");
    }

    public function email_mod(){
        $this->grocery_crud->set_table('tbl_mod_email');

        $this->grocery_crud->display_as('value','Email');
        $this->grocery_crud->required_fields('value');

        $this->grocery_crud->set_rules('value','Email','valid_email');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_delete();

        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->in_prize_email = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/mod/mod_detail");
        $this->load->view("cms/closure");
    }

    // participants management
    public function participants(){

        $this->grocery_crud->set_table('tbl_user');

        $this->grocery_crud->display_as('email','Email')
        ->display_as('nid', 'Número de Cédula')
        ->display_as('name','Nombre/s')
        ->display_as('lastname','Apellido/s')
        ->display_as('cellphone','Celular')
        ->display_as('phone','Teléfono')
        ->display_as('phone_area_code','Indicativo')
        ->display_as('nid_img_front','Imágen de cédula frente')
        ->display_as('nid_img_back','Imágen de cédula atras')
        ->display_as('created_at','Fecha de creación')
        ->display_as('last_login','Última conexión')
        ->display_as('verified','Usuario verificado');

        $this->grocery_crud->set_field_upload('nid_img_front',$this->user_nid_imgs_folder);
        $this->grocery_crud->set_field_upload('nid_img_back',$this->user_nid_imgs_folder);

        $this->grocery_crud->unset_fields('created_at','last_login');

        // thumbnail mod
        $this->grocery_crud->callback_column('nid_img_front', array($this,'_generateImgThumbnail'));
        $this->grocery_crud->callback_column('nid_img_back', array($this,'_generateImgThumbnail'));

        $this->grocery_crud->callback_column('email', array($this, '_uppercase_text'));
        $this->grocery_crud->callback_column('nid', array($this, '_uppercase_text'));
        $this->grocery_crud->callback_column('name', array($this, '_uppercase_text'));
        $this->grocery_crud->callback_column('lastname', array($this, '_uppercase_text'));
        $this->grocery_crud->callback_column('verified', array($this, '_uppercase_text'));


        $output = $this->grocery_crud->render();

        $output->in_users = true;
        $output->in_users_participants = true;


        $this->load->view("cms/opener", $output);
        $this->load->view("cms/users/participants_users");
        $this->load->view("cms/closure");
    }

    public function _generateImgThumbnail($value, $row){
        $img = '<div class="text-left"><a href="' . base_url() . 'uploads/nid_imgs/' . $value . '" class="image-thumbnail"><img src="'. base_url('timthumb.php') .'?src=' . base_url() . 'uploads/nid_imgs/' . $value . '&h=50&zc=2" height="50px"></a></div>';
        return $img;
    }

    // promos management
    public function promos(){

        $this->grocery_crud->set_table('tbl_promo');
        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $this->grocery_crud->display_as('title','Titulo')
        ->display_as('media', 'Archivo (jpg, png, gif, swf)')
        ->display_as('available_from','Disponible desde')
        ->display_as('available_until','Disponible hasta')
        ->display_as('order','Orden');

        $this->grocery_crud->field_type('order','dropdown', $this->makeDropdownNumbers(0,50) );

        $this->grocery_crud->callback_add_field('link',function () {
            return '<input type="text" maxlength="50" placeholder="ejemplo: http://google.com.co"  value="" name="link">';
        });

        $this->grocery_crud->field_type('link', 'string');

        $this->grocery_crud->unset_fields('created_at');

        $this->grocery_crud->set_field_upload('media','assets/uploads/files');

        $this->grocery_crud->required_fields('title','media', 'available_from', 'available_until', 'order' );

        $this->grocery_crud->callback_before_upload(array($this,'promos_validation'));
        $output = $this->grocery_crud->render();

        $output->in_promos = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/promos/list");
        $this->load->view("cms/closure");
    }

    // helper function for numeric dropdown grocery crud
    public function promos_validation( $files_to_upload, $field_info ){


        $allowed_extensions = array('jpg', 'jpeg', 'png', 'swf');

        foreach( $files_to_upload as $file ){

            $file_ext = end(explode('.', $file['name']) );

            if( !in_array( $file_ext , $allowed_extensions) ){

                return "Solo son permitidos archivos de tipo jpg, swf, png";
            }
        }

        return true;

    }

    public function cms_users(){

        $this->grocery_crud->set_table('tbl_cms_user');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $this->grocery_crud->display_as('username','Usuario')
        ->display_as('password','Contraseña');

        $this->grocery_crud->field_type('password', 'password');

        $this->grocery_crud->callback_before_insert(array($this,'encrypt_password_callback'));
        $this->grocery_crud->callback_before_update(array($this,'encrypt_password_callback'));

        $this->grocery_crud->required_fields('username','password');
        $this->grocery_crud->unset_fields('created_at');

        $output = $this->grocery_crud->render();

        $output->in_users = true;
        $output->in_users_cms = true;
        $output->custom_js[] = 'js/cms_users.js';

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/users/cms_users');
        $this->load->view('cms/closure');

    }

    // cadenas
    public function stores(){

        $this->grocery_crud->set_table('tbl_store');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $this->grocery_crud->display_as('fk_city_id','Ciudad')
        ->display_as('name', 'Nombre');

        $this->grocery_crud->required_fields('fk_city_id','name');

        $this->grocery_crud->set_relation('fk_city_id','tbl_city','name');

        $output = $this->grocery_crud->render();

        $output->in_stores = true;
        $output->in_stores_stores = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/store');
        $this->load->view('cms/closure');
    }

    public function sellpoints(){

        $this->grocery_crud->set_table('vw_sells_points');
        $this->grocery_crud->set_primary_key('sellpoint_id');

        $this->grocery_crud->columns('city','store','sellpoint_name');

        $this->grocery_crud->display_as('city','Ciudad')
        ->display_as('store','Cadena')
        ->display_as('sellpoint_name','Punto de venta');

        $this->grocery_crud->add_action('Eliminar', 'assets/grocery_crud/themes/flexigrid/css/images/close.png', 'cms/delete_sellpoint');
        $this->grocery_crud->add_action('Editar', 'assets/grocery_crud/themes/flexigrid/css/images/edit.png', 'cms/edit_sellpoint');

        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_read();
        $this->grocery_crud->unset_edit();
        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $output = $this->grocery_crud->render();

        $output->in_stores = true;
        $output->in_stores_sellpoints = true;

        $output->custom_js[] = 'js/cms_sellpoints.js';


        $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/sell_points');
        $this->load->view('cms/closure');
    }

    public function edit_sellpoint( $sellpoint_id ){

        $output = array(
            'in_stores' => true,
            'in_stores_sellpoints' => true,
            'cities' => $this->main->getStoreAvailableCities(),
            'sellpoint' => $this->main->getSellPointById( $sellpoint_id )
            );

        $output['stores'] = $this->main->getCityStores( $output['sellpoint']->city_id );

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/edit_sellpoint');
        $this->load->view('cms/closure');
    }

    public function add_sellpoint(){

        $output = array(
            'in_stores' => true,
            'in_stores_sellpoints' => true,
            'cities' => $this->main->getStoreAvailableCities()
            );

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/add_sellpoint');
        $this->load->view('cms/closure');
    }

    public function update_sellpoint(){

        $data = array(
            'name' => $this->input->post('sellpoint'),
            'fk_store_id' => $this->input->post('storeId')
            );

        $this->main->updateSellpoint( $this->input->post('sellpoint_id'), $data );

        redirect( base_url() . 'cms/sellpoints' );
    }

    public function delete_sellpoint(){
        $r = $this->main->deleteSellpoint( $this->input->post('sellpoint_id') );
        echo json_encode( array('result' => $r));
    }

    public function save_sellpoint($sellpoint_id){
        $data = array(
            'name' => $this->input->post('sellpoint'),
            'fk_store_id' => $this->input->post('storeId')
            );

        $this->main->saveSellpoint( $data );

        redirect( base_url() . 'cms/sellpoints' );
    }

    function encrypt_password_callback( $post_array ){

        $post_array['password'] = $this->encrypt->encode($post_array['password']);
        return $post_array;
    }

    private function makeDropdownNumbers( $start, $end ){

        $result = array();

        for( $start ; $start <= $end; $start++ ){
            $result[$start] = $start;
        }

        return $result;

    }

    public function get_city_stores(){
        $r = $this->main->getCityStores( $this->input->get('city_id'));

        if( count($r > 0 ) ){
            echo json_encode( array('success' => true,'data' => $r ));
        } else {
            echo json_encode( array('success' => false));
        }

    }

    public function login(){

        if( $this->session->userdata('logged_in') ){
            redirect('cms');
        }

        $this->load->view('cms/login');
    }

    public function check_login_credentials(){

        $result = $this->main->logInCmsUser( $_POST );
        echo json_encode( array('result' => $result) );
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('cms');
    }
    /*tvs*/
    public function tv(){

        $this->grocery_crud->set_table('tbl_tv');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $this->grocery_crud->display_as('id','Identificacion unica')
        ->display_as('reference','Referencia del tv')
        ->display_as('description', 'Descripcion del televisor')
        ->display_as('created_at','Fecha de creacion');


        $this->grocery_crud->unset_fields('created_at');
        $this->grocery_crud->required_fields('reference');
        $this->grocery_crud->callback_before_upload(array($this,'images_validation'));
        $output = $this->grocery_crud->render();

        $output->in_tv = true;
        //$output->in_stores_stores = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/tvs/tv');
        $this->load->view('cms/closure');
    }

    public function images_validation( $files_to_upload, $field_info )
    {


        $allowed_extensions = array('jpg', 'jpeg', 'png');

        foreach( $files_to_upload as $file )
        {

            $file_ext = end(explode('.', $file['name']) );

            if( !in_array( $file_ext , $allowed_extensions) )
            {

                return "Solo son permitidos archivos de tipo jpg, png";
            }
        }

        return true;

    }

    public function excel_validation($files_to_upload, $field_info){
        $allowed_extensions = array('xls', 'xlsx' );

        foreach( $files_to_upload as $file )
        {

            $file_ext = end(explode('.', $file['name']) );

            if( !in_array( $file_ext , $allowed_extensions) )
            {

                return "Solo son permitidos archivos de tipo excel";
            }
        }

        return true;
    }


    /*premios*/

    public function delete_prize_package($id){

        $this->main->deletePrizePackage($id);
        redirect('cms/prize');
    }

    public function prize()
    {


        // THE QUANTY FIELD IS USELESS, THE QUNATY IS BEING MANAGED ON PRIZE DETAIL TABLE

        $this->grocery_crud->set_table('vw_prize_beneficts');

        $this->grocery_crud->set_primary_key('id');

        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_read();
        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_edit();
        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        // add custom actions
        $this->grocery_crud->add_action('Eliminar', 'assets/grocery_crud/themes/flexigrid/css/images/close.png', 'cms/delete_prize_package');
        $this->grocery_crud->add_action('Editar', 'assets/grocery_crud/themes/flexigrid/css/images/edit.png', 'cms/edit_prize_package');

        $this->grocery_crud->display_as('reference','Referencia Tv')
        ->display_as('title', 'titulo del beneficio')
        ->display_as('image', 'imagen del beneficio')
        ->display_as('description','Descripcion del beneficio')
        ->display_as('created_at','fecha de creacion')
        ->display_as('quanty','Cantidad')
        ->display_as('terms','Terminos y condiciones')
        ->display_as('has_promo_code', 'Código del beneficio')
        ->display_as('beneficts', 'Beneficios');

        $this->grocery_crud->field_type('has_promo_code','dropdown',array(
            'no'    => 'No',
            'yes'   => 'Si'
            ));

        $this->grocery_crud->columns('reference','title','image','description','created_at', 'has_promo_code', 'beneficts');

        $this->grocery_crud->set_field_upload('image','assets/uploads/files');

        $this->grocery_crud->unset_fields('created_at','quanty');

        $this->grocery_crud->set_relation('fk_tv_id','tbl_tv','reference');

        $this->grocery_crud->callback_add_field('fk_tv_id',function () {
            return $this->reference_validation();
        });

        $this->grocery_crud->callback_edit_field('fk_tv_id',function () {
            return $this->reference_validation_edit();
        });

        // custom callbacks
        $this->grocery_crud->callback_insert(array($this,'insert_custom_tbl_prize_tbl_store'));
        $this->grocery_crud->callback_update(array($this, 'update_custom_tbl_prize_tbl_store'));

        $output = $this->grocery_crud->render();

        $action = $this->grocery_crud->getState();

        $angular_data = null;


        /*if( $action == 'add' || $action == 'edit' ){
            $output->output = str_replace( '<div class="pDiv">' , '<div class="pDiv">' . $this->replace_content($angular_data) , $output->output);
        }*/



        $output->in_prize = true;
        $output->in_prize_prizes = true;


        $this->load->view('cms/opener',$output);
        $this->load->view('cms/prizes/prize');
        $this->load->view('cms/closure');
    }

    public function add_prize_package(){

        $data = array(
            'tvs'                     => $this->main->get_available_tv_prizes(),
            // 'cities'                  => $this->main->getStoreAvailableCities(),
            'prize_items'             => $this->main->getPrizeItems(),
            'in_prize'                => true,
            'in_prize_prizes'         => true,
            // 'cities_store_sellpoints' => $this->main->getCitySellpointsStoreAssoc()
        );

        $this->load->view('cms/opener',$data);
        $this->load->view('cms/prizes/prize_package');
        $this->load->view('cms/closure');
    }

    public function edit_prize_package( $prize_id ){

        $prize_items_ids = array();
        $prize_sellpoint_ids = array();

        foreach( $this->main->getPrizeElements($prize_id) as $pe ){
            $prize_items_ids[] = $pe->fk_prize_detail_id;
        }


        /* foreach( $this->main->getPrizePackageSellPoints( $prize_id ) as $sp ){
            $prize_sellpoint_ids[] = $sp->fk_sells_point_id;
        }*/


        $data = array(
            'prize_package'           => $this->main->getPrizePackageById($prize_id),
            'tvs'                     => $this->main->get_available_tv_prizes_update($prize_id),
            // 'cities'                  => $this->main->getStoreAvailableCities(),
            'prize_items'             => $this->main->getPrizeItems(),
            'in_prize'                => true,
            'in_prize_prizes'         => true,
            // 'cities_store_sellpoints' => $this->main->getCitySellpointsStoreAssoc(),
            'prize_items_ids'         => $prize_items_ids,
            // 'prize_sellpoint_ids'     => $prize_sellpoint_ids
        );

        $this->load->view('cms/opener',$data);
        $this->load->view('cms/prizes/edit_prize_package');
        $this->load->view('cms/closure');
    }

    public function save_prize_package(){

        $prize_package = array(
            'fk_tv_id'        => $this->input->post('fk_tv_id'),
            'title'           => $this->input->post('title'),
            'description'     => $this->input->post('description'),
            'terms'           => $this->input->post('terms'),
            'has_promo_code'  => $this->input->post('has_promo_code')
        );

        if(  $_FILES['image']['name'] != '' ){


            $rf = $this->model_files->upload_file('image', './assets/uploads/files', 'jpg|jpeg|png' );

            if( $rf['success'] === true ){

                $prize_package['image'] = $rf['data']['file_name'];
            } else {
                redirect( base_url() . 'site/set_purchase_data?error=' . $rf['error'] );
            }

        }

        $prize_id = $this->main->savePrizePackageData( $prize_package );

        $this->prizeItemsAssoc($prize_id);
        // $this->prizeSellpointsAssoc($prize_id);

        redirect('cms/prize');

    }

    public function update_prize_package(){

        $prize_package = array(
            'fk_tv_id'        => $this->input->post('fk_tv_id'),
            'title'           => $this->input->post('title'),
            'description'     => $this->input->post('description'),
            'terms'           => $this->input->post('terms'),
            'has_promo_code'  => $this->input->post('has_promo_code')
        );

        if(  $_FILES['image']['name'] != '' ){


            $rf = $this->model_files->upload_file('image', './assets/uploads/files', 'jpg|jpeg|png' );

            if( $rf['success'] === true ){

                $prize_package['image'] = $rf['data']['file_name'];
            } else {
                redirect( base_url() . 'site/set_purchase_data?error=' . $rf['error'] );
            }

        }

        $prize_id = $this->input->post('prize_id');

        $this->main->updatePrizePackage($prize_id, $prize_package);
        $this->prizeItemsAssoc($prize_id, true);
        // $this->prizeSellpointsAssoc($prize_id, true);

        redirect('cms/edit_prize_package/' . $prize_id);

    }

    private function prizeItemsAssoc( $prize_id, $clean_up = false ){

        if( $clean_up === true ){
            $this->main->deletePrizeElements( $prize_id );
        }

        // prize items assoc
        foreach( $this->input->post('prize_items') as $prize_detail_id ){
            $prize_elements[] = array(
                'fk_prize_id'        => $prize_id,
                'fk_prize_detail_id' => $prize_detail_id
            );
        }

        $this->main->savePrizeElements($prize_elements);
    }

    private function prizeSellpointsAssoc( $prize_id, $clean_up = false ){

        if( $clean_up === true ){

            $this->main->deletePrizeStores( $prize_id );

        }

        $sell_points_ids = explode(',', $this->input->post('sellpointIds') );

        foreach( $sell_points_ids as $sp_id ){
            $prize_stores_data[] = array(
                'fk_prize_id'       => $prize_id,
                'fk_sells_point_id' => $sp_id
            );
        }

        $this->main->insertPrizeStores( $prize_stores_data );
    }

    public function prize_detail(){

        $this->grocery_crud->set_table('tbl_prize_detail');

        $this->grocery_crud->set_field_upload('image','assets/uploads/files');
        $this->grocery_crud->unset_fields('created_at','last_login');

        $this->grocery_crud->display_as('title', 'Titulo del beneficio')
        ->display_as('image', 'Imágen( está será la imágen del beneficio)')
        ->display_as('description', 'Descripción (esto sólo se verá en CMS)')
        ->display_as('has_list_of_codes', '¿Cuenta con lista de códigos?')
        ->display_as('available_quanty', 'Cantidad')
        ->display_as('created_at', 'Fecha de creación')
        ->display_as('reclaim_description', 'Pasos a seguir para relclamar el beneficio (se verá en el email)')
        ->display_as('available_from', 'Disponible desde')
        ->display_as('available_until', 'Disponible hasta');

        $this->grocery_crud->columns('title','image','description', 'has_list_of_codes', 'available_quanty', 'available_from', 'available_until' ,'created_at');

        $this->grocery_crud->field_type('has_list_of_codes','dropdown',array(
            'yes' => 'Si',
            'no'  => 'No'
        ));

        $this->grocery_crud->required_fields('title','image', 'has_list_of_codes' );
        $this->grocery_crud->callback_before_upload(array($this,'images_validation'));

        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->in_prize_detail = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/prizes/prize_detail');
        $this->load->view('cms/closure');

    }



    public function prize_codes(){

        $action = $this->grocery_crud->getState();


        $this->grocery_crud->set_table('tbl_prize_codes');
        if( $action != 'edit' && $action != 'list' ){
            $this->grocery_crud->set_field_upload('value', 'assets/uploads/files/');
            $this->grocery_crud->callback_before_upload(array($this,'excel_validation'));
        }



        switch( $action ){
            case 'add':

                $this->grocery_crud->unset_fields('created_at','available');
                $this->grocery_crud->display_as('value', 'Archivo (excel xls, xlsx)');
                break;

            case 'edit':
                $this->grocery_crud->unset_fields('created_at');
                $this->grocery_crud->display_as('value', 'Código');
                break;

            default:
                $this->grocery_crud->display_as('value', 'Código');
                break;
        }

        $this->grocery_crud->set_relation('fk_prize_detail_id','tbl_prize_detail','title' ,array('has_list_of_codes' => 'yes'));

        $this->grocery_crud->display_as('fk_prize_detail_id', 'Beneficio')
        ->display_as('available', 'Disponible')
        ->display_as('created_at', 'Fecha de creación');

        $this->grocery_crud->field_type('available','dropdown',array(
            'yes' => 'Si',
            'no'  => 'No'
        ));


        $this->grocery_crud->callback_insert(array($this,'insert_custom_prize_codes'));

        $this->grocery_crud->required_fields('fk_prize_detail_id','value' );

        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->in_prize_codes = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/prizes/prize_codes');
        $this->load->view('cms/closure');
    }

    public function insert_custom_prize_codes($post_array){
        $prize_detail_id = $post_array['fk_prize_detail_id'];

        // insert codes
        $this->load->model('excel_ops');
        $this->excel_ops->setPrizeCodes( $post_array['fk_prize_detail_id'], $post_array['value'] );

        // updates benefit quanty
        $this->update_benefit_quanty( $post_array['fk_prize_detail_id'] );

        return true;
    }

    public function insert_custom_lg_codes($post_array){

        $this->load->model('excel_ops');
        $this->excel_ops->setLgCodes( $post_array['value'] );

        return true;
    }


    public function insert_custom_tbl_prize_tbl_store($post_array)
    {

        $store_id=$post_array['storeId'];
        unset($post_array['storeId']);

        $this->db->insert('tbl_prize',$post_array);
        $prize_id = $this->db->insert_id();

        foreach ($store_id as $key => $value) {
          $data[] = array(
             'fk_prize_id' => $prize_id,
             'fk_store_id' => $store_id[$key]
             );
        }

       $this->main->insertPrizeStores($data);
    }

    public function update_custom_tbl_prize_tbl_store($post_array, $primary_key){

        $store_id = $post_array['storeId'];
        unset($post_array['storeId']);
        $prize_id = $primary_key;

        foreach ($store_id as $key => $value) {
          $data[] = array(
             'fk_prize_id' => $prize_id,
             'fk_store_id' => $value
             );
        }


        $this->main->deletePrizeStores($prize_id);
        $this->main->insertPrizeStores($data);

        return $this->db->update('tbl_prize',$post_array,array('id' => $primary_key));
    }

    public function reference_validation()
    {

        $data['tvs']= $this->main->get_available_tv_prizes();
        $print= "<select name='fk_tv_id' id='field-fk_tv_id' data-placeholder='Seleccionar Referencia'>";
        $print.="<option value=''>Selecciona una Referencia</option>";
        foreach ($data['tvs'] as $row)
        {
            $print.= "<option value='".$row->id."'>".$row->reference."</option>";
        }
        $print.= "</select>";
        return $print;
    }

    public function replace_content( $angular_data = null )
    {
        $output = array(
            'cities'       => $this->main->getStoreAvailableCities(),
            'angular_data' => $angular_data
            );

       /* $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/add_sellpoint');*/
        $cadena = $this->load->view('cms/prizes/add_store_prize', $output, true);

        return $cadena;
    }

    public function prize_requests(){

        $this->grocery_crud->set_table('vw_prize_requests');
        $this->grocery_crud->set_primary_key('prize_application_id');

        $this->grocery_crud->columns('name','lastname','nid', 'prize_request_qty', 'store_name', 'city_name', 'tv_references', 'code_lg', 'promotional_code_txt', 'request_date', 'status');

        $this->grocery_crud->display_as('name','Nombre/s')
            ->display_as('lastname', 'Apellido/s')
            ->display_as('nid', 'No de Cédula')
            ->display_as('prize_request_qty', 'Cantidad de tvs comprados')
            ->display_as('request_date', 'Fecha de solicitud')
            ->display_as('status', 'Estado')
            ->display_as('store_name', 'Cadena')
            ->display_as('city_name', 'Ciudad')
            ->display_as('tv_references', 'Referencia/s de tv')
            ->display_as('code_lg', 'Código de beneficio LG')
            ->display_as('promotional_code_txt', 'Código promocional');

        $this->grocery_crud->field_type('status','dropdown',array(
            'email_confirmacion' => 'Email de confirmación',
            'en_revision'        => 'En Revisión',
            'no_aprobado'        => 'No aprobado',
            'aprobado'           => 'Aprobado',
            'entregado'          => 'Entregado'
            ));

        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_read();
        $this->grocery_crud->unset_edit();

        $this->grocery_crud->order_by('request_date','DESC');

        $this->grocery_crud->add_action('Ver Detalle', 'assets/grocery_crud/themes/flexigrid/css/images/magnifier.png', 'cms/prize_request_detail');

        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->in_prize_requests = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/prizes/prize_requests');
        $this->load->view('cms/closure');
    }

    public function prize_request_detail($prize_application_id){

        $purchaseData = $this->main->getPurchaseDataByPrizeAppId( $prize_application_id );

        $data = array(
            'prize_detail'         => $this->main->getPrizeApplicationDetail( $prize_application_id ),
            'in_prize'             => true,
            'in_prize_requests'    => true,
            'promotional_code_txt' => $purchaseData->promotional_code_txt,
            'custom_js'            => array('js/prize_req_detail.js')
        );

        $this->load->view('cms/opener',$data);
        $this->load->view('cms/prizes/prize_request_detail');
        $this->load->view('cms/closure');
    }

    public function set_prize_request_status(){


        if( $this->input->post('sendApprovalMail') == 1 ){
            // send approval email

            $prize_application_id = $this->input->post('prize_application_id');
            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            $prizes_view = array();

            // previous assignment codes variables
            $exclusion_codes_id = array();

            foreach( $appRequestDetail as $ar ){

                $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id, array(
                        'prize_date_availability'       => 1,
                        'prize_bool_quantity_available' => 1
                    ));

                foreach( $prize_detail as $pd ){
                    $prizes_view[] = $pd;

                    if( $pd->prize_detail_has_list_of_codes == 'yes' ){

                        /*
                         * we need to CHECK if this request has a previous code assignmet in order
                         * to retrieve the assigned codes or give new ones
                         */

                        if( $this->main->hasCodesAssigned( $appRequestDetail[0]->purchase_data_id ) ){

                            $code = $this->main->retrievePreviousCodes( $appRequestDetail[0]->purchase_data_id, $exclusion_codes_id );
                            $exclusion_codes_id[] = $code->id;


                        } else {
                            $code = $this->main->pickUpCode( $pd->prize_detail_id, $appRequestDetail[0]->purchase_data_id );

                            if( $appRequestDetail[0]->prize_status != 'aprobado' ){
                                // code substraction
                                // code available quanty update
                                $this->main->updateBenefictQuanty( $pd->prize_detail_id );
                            }
                        }

                        $prizes_view[ count($prizes_view) -1 ]->code = $code->value;
                    } else {
                        if( $appRequestDetail[0]->prize_status != 'aprobado' ){
                            // substraction prize available quanty
                            $this->main->substractBenefitQuanty( $pd->prize_detail_id );
                        }
                    }
                }

                // TODO: resta de inventario
                /*if( $appRequestDetail[0]->prize_status != 'aprobado' ){
                    $tv_id_associate= $this->main->getPackageTvbyId($ar->tv_id);
                    $value_rest= $tv_id_associate->quanty;
                    $update_quanty= $value_rest - 1;

                    $this->main->update_quanty_prize($ar->tv_id,$update_quanty);
                }*/


            }

            $data = array(
                'prizes_view' => $prizes_view
                );

            $mail_body = $this->load->view('emails/email-accepted', $data, true);

            $r = $this->model_mails->singleEmail( $appRequestDetail[0]->email, $mail_body, 'LG - masqueuntv.com Solicitud aprobada' , $this->main->getModEmail() );
        }


        $r = $this->main->setPrizeApplicationStatus( $this->input->post('prize_application_id'), $this->input->post('request_status') );
        redirect( base_url() . 'cms/prize_requests' );
    }

    private function reference_validation_edit(){

        $prize_id = $this->grocery_crud->getStateInfo()->primary_key;

        $data = $this->main->get_available_tv_prizes_update( $prize_id );

        $print= "<select name='fk_tv_id' id='field-fk_tv_id' data-placeholder='Seleccionar Referencia'>";
        $print.="<option value=''>Selecciona una Referencia</option>";

        $selected = '';

        foreach ($data as $row)
        {
            $row->prize_id == $prize_id ? $selected = 'selected' : $selected = '';
            $print.= "<option $selected value='".$row->id."'>".$row->reference."</option>";
        }
        $print.= "</select>";
        return $print;
    }

    public function promotor(){

        $this->grocery_crud->set_table('tbl_promotor');

        $this->grocery_crud->display_as('document', 'Cédula')
        ->display_as('name', 'Nombre')
        ->display_as('lastname', 'Apellido');

        $this->grocery_crud->order_by('name','ASC');
        $this->grocery_crud->required_fields('document','name','lastname');

        $output = $this->grocery_crud->render();
        $output->in_promotor = true;
        $output->in_promotor_admin = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/default_render');
        $this->load->view('cms/closure');

    }

    public function promotor_bulk_upload(){

        $output = array(
            'in_promotor'      => true,
            'in_promotor_bulk' => true
        );

        $this->load->view('cms/opener', $output);
        $this->load->view('cms/promotors/bulk_upload');
        $this->load->view('cms/closure');
    }

    public function parse_promotor_bulk_upload(){

        $this->load->model('excel_ops');

        if(  $_FILES['promotorFile']['name'] != '' ){


            $rf = $this->model_files->upload_file('promotorFile', './assets/uploads/files', 'xls|xlsx' );

            if( $rf['success'] === true ){

                $file_name = $rf['data']['file_name'];
                $this->excel_ops->storePromotors($file_name);

                redirect('cms/promotor_bulk_upload?success');

            } else {
                redirect( base_url() . 'cms/promotor_bulk_upload?error=' . $rf['error'] );
            }

        }
    }

    public function codes_report(){

        $this->grocery_crud->set_table('vw_codes_report');
        $this->grocery_crud->set_primary_key('purchase_data_id');

        $this->grocery_crud->columns('name','lastname', 'nid', 'code_lg', 'prize_request_qty', 'tv_references', 'used_codes', 'assign_date' );

        $this->grocery_crud->display_as('name','Nombre/s')
            ->display_as('lastname', 'Apellido/s')
            ->display_as('nid', 'No de Cédula')
            ->display_as('prize_request_qty', 'Cantidad de tvs comprados')
            ->display_as('tv_references', 'Referencia/s de tv')
            ->display_as('code_lg', 'Código de beneficio LG')
            ->display_as('used_codes', 'Códigos otorgados')
            ->display_as('assign_date', 'Fecha de asignación del código');

        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_add();
        // $this->grocery_crud->unset_read();
        $this->grocery_crud->unset_edit();

        $this->grocery_crud->order_by('assign_date','DESC');
        $this->grocery_crud->unset_fields('purchase_data_id');

        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->in_codes_report = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/default_render');
        $this->load->view('cms/closure');

    }

    public function full_request_report(){

        $this->grocery_crud->set_table('vw_full_prize_requests');
        $this->grocery_crud->set_primary_key('prize_application_id');

        $this->grocery_crud->field_type('status','dropdown',array(
            'email_confirmacion' => 'EMAIL DE CONFIRMACIÓN',
            'en_revision'        => 'EN REVISIÓN',
            'no_aprobado'        => 'NO APROBADO',
            'aprobado'           => 'APROBADO',
            'entregado'          => 'ENTREGADO'
            ));

        // CUSTOM CONSTRAINTS
        // http://stackoverflow.com/questions/6552821/codeigniter-parentheses-in-dynamic-active-record-query

        if( $this->session->userdata('reference_filter') ){

            $reference_filter_constraints = '';

            foreach( (array) $this->session->userdata('reference_filter') as $key => $rf ){
                if( $key == 0 ){
                    $reference_filter_constraints .= " tv_ids LIKE '%$rf%' ";
                } else {
                    $reference_filter_constraints .= " or tv_ids LIKE '%$rf%' ";
                }

            }

        }


        if( $this->session->userdata('benefict_filter') ){

            $benefict_filter_constraints = '';

            foreach( (array) $this->session->userdata('benefict_filter') as $key => $rf ){
                if( $key == 0 ){
                    $benefict_filter_constraints .= " prize_ids LIKE '%$rf%' ";
                } else {
                    $benefict_filter_constraints .= " or prize_ids LIKE '%$rf%' ";
                }

            }

        }

        if( !empty( $reference_filter_constraints ) ){
            $this->grocery_crud->where( "($reference_filter_constraints)" );
        }

        if( !empty( $benefict_filter_constraints ) ){
            $this->grocery_crud->where( "($benefict_filter_constraints)" );
        }

        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_read();
        $this->grocery_crud->unset_edit();

        $this->grocery_crud->order_by('request_date','DESC');
        $this->grocery_crud->order_by('request_date','DESC');

        $columns = array();
        foreach( $this->session->userdata('full_request_report') as $key => $c ){
            $columns[] = $key;
        }

        $this->grocery_crud->columns( $columns );

        // $this->grocery_crud->add_action('Ver Detalle', 'assets/grocery_crud/themes/flexigrid/css/images/magnifier.png', 'cms/prize_request_detail');

        $this->grocery_crud->display_as('name','Nombre/s')
            ->display_as('lastname', 'Apellido/s')
            ->display_as('nid', 'No de Cédula')
            ->display_as('prize_request_qty', 'Cantidad de tvs comprados')
            ->display_as('request_date', 'Fecha de solicitud')
            ->display_as('status', 'Estado')
            ->display_as('sellpoint_name', 'Punto de venta')
            ->display_as('store_name', 'Cadena')
            ->display_as('city_name', 'Ciudad')
            ->display_as('tv_references', 'Referencia tv / serial')
            ->display_as('tv_references_list', 'Referencia/s')
            ->display_as('tv_serials_list', 'Serial/es')
            ->display_as('code_lg', 'Código de beneficio LG')
            ->display_as('promotional_code_txt', 'Campo opcional')
            ->display_as('beneficts', 'Beneficios')
            ->display_as('cellphone', 'Celular')
            ->display_as('phone', 'Teléfono')
            ->display_as('phone_area_code', 'Código de area')
            ->display_as('nid_img_front', 'Cédula - frente')
            ->display_as('nid_img_back', 'Cédula - atrás')
            ->display_as('bill_img', 'Factura')
            ->display_as('used_codes', 'Códigos otorgados')
            ->display_as('purchase_date', 'Fecha de compra')
            ->display_as('purchase_amount', 'Precio de tvs');

        $this->grocery_crud->order_by('request_date','DESC');

        $this->grocery_crud->callback_column('nid_img_front',array($this,'_callback_full_report_img'));
        $this->grocery_crud->callback_column('nid_img_back',array($this,'_callback_full_report_img'));
        $this->grocery_crud->callback_column('bill_img',array($this,'_callback_full_report_bill_img'));

        $this->grocery_crud->callback_column('tv_references', array($this, '_full_text'));
        $this->grocery_crud->callback_column('tv_serials_list', array($this, '_full_text'));
        $this->grocery_crud->callback_column('tv_references_list', array($this, '_full_text'));
        $this->grocery_crud->callback_column('beneficts', array($this, '_full_text'));
        $this->grocery_crud->callback_column('used_codes', array($this, '_full_text'));

        $this->grocery_crud->callback_column('name', array($this, '_full_text'));
        $this->grocery_crud->callback_column('lastname', array($this, '_full_text'));
        $this->grocery_crud->callback_column('email', array($this, '_full_text'));
        $this->grocery_crud->callback_column('sellpoint_name', array($this, '_full_text'));
        $this->grocery_crud->callback_column('store_name', array($this, '_full_text'));
        $this->grocery_crud->callback_column('city_name', array($this, '_full_text'));


        $this->grocery_crud->add_action('Eliminar Solicitud', 'assets/grocery_crud/themes/flexigrid/css/images/close.png', 'cms/delete_purchase_request');
        $this->grocery_crud->add_action('Reenviar beneficios', 'assets/grocery_crud/themes/flexigrid/css/images/load.png', 'cms/resend_beneficts');
        // $this->grocery_crud->add_action('Aprobar/Denegar Solicitud', 'assets/grocery_crud/themes/flexigrid/css/images/success.png', '', 'admin-approve-deny', array($this, '_admin_approve_deny_request') );
        $this->grocery_crud->add_action('Historia', 'assets/grocery_crud/themes/flexigrid/css/images/time.png', 'cms/request_historic');
        $this->grocery_crud->add_action('Editar Solicitud (datos básicos)', 'assets/grocery_crud/themes/flexigrid/css/images/edit.png', 'cms/edit_request');
        $this->grocery_crud->add_action('Editar Solicitud (completo)', 'assets/grocery_crud/themes/flexigrid/css/images/edit.png', '','admin-edit-request', array($this, '_admin_edit_request') );


        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->full_request_report = true;

        $output->beneficts  = $this->main->getPrizeItems();
        $output->references = $this->main->getTvs();

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/prizes/full_prize_request_list');
        $this->load->view('cms/closure');
    }

    public function _admin_edit_request( $primary_key, $row ){
        return site_url( 'site/register_update?token=' . $this->uriencryption->encode($row->prize_application_id) . '&admin_mode' );
    }

    public function set_request_filters(){

        if( isset( $_POST['filters'] ) ){
            $this->session->set_userdata('full_request_report', $_POST['filters'] );
        } else {
            $this->session->unset_userdata('filters');
        }

        if( isset( $_POST['benefict_filter'] ) ){
            $this->session->set_userdata('benefict_filter', $_POST['benefict_filter'] );
        } else {
            $this->session->unset_userdata('benefict_filter');
        }

        if( isset( $_POST['reference_filter'] ) ){
            $this->session->set_userdata('reference_filter', $_POST['reference_filter'] );
        } else {
            $this->session->unset_userdata('reference_filter');
        }

        redirect('cms/full_request_report');
    }

    public function _callback_full_report_img( $value, $row ){

        return "<a target='_BLANK' href='uploads/nid_imgs/$value' >$value</a>";
    }

    public function _callback_full_report_bill_img( $value, $row ){

        return "<a target='_BLANK' href='$value' >$value</a>";
    }

    public function delete_purchase_request( $prize_application_id ){

        $this->main->deletePurchaseDataByPrizeApplicationId( $prize_application_id );
        $this->session->set_flashdata('flash_message', 'Se ha eliminado la solicitud.');
        redirect('cms/full_request_report');
    }

    public function edit_request( $prize_application_id ){

        $data = array(
            'request'              => $this->main->getFullPrizeRequestByPrizeApplicationId( $prize_application_id ),
            'full_request_report'  => true,
            'in_prize'             => true,
            'prize_detail'         => $this->main->getPrizeApplicationDetail( $prize_application_id ),
            'custom_js'            => array('js/prize_req_detail.js')
        );

        $data['request']->purchase_amount = str_replace( "$" , "" , $data['request']->purchase_amount);

        $this->load->view('cms/opener', $data);
        $this->load->view('cms/prizes/edit_request');
        $this->load->view('cms/closure');
    }

    public function update_purchase_request(){

        /*krumo($_POST);
        krumo($_FILES);*/

        $user_data = array(
         'name'            => $this->input->post('name'),
         'lastname'        => $this->input->post('lastname'),
         'cellphone'       => $this->input->post('cellphone'),
         'phone'           => $this->input->post('phone'),
         'phone_area_code' => intval($this->input->post('phone_area_code')),
         'email'           => $this->input->post('email'),
         'nid'             => $this->input->post('nid')
        );

        $user_data = $this->intent_upload_user_files($user_data);
        $this->main->updateUser( $this->input->post('user_id'), $user_data );

        $purchase_data = array(
            'fk_promotor_id'  => $this->input->post('promotor_id'),
            'purchase_amount' => $this->input->post('purchase_amount')
        );

        if( empty($purchase_data['fk_promotor_id']) ){
            $purchase_data['fk_promotor_id'] = null;
        }

        $purchase_data = $this->intent_upload_bill_file($purchase_data);
        $this->main->updatePurchaseData( $this->input->post('purchase_data_id'), $purchase_data );

        $this->session->set_flashdata('flash_message', 'Registro actualzado.');
        redirect('cms/full_request_report');


    }

    private function intent_upload_user_files( $data ){

        // intents upload files
        if( $_FILES['nid_img_front']['name'] != '' ){
            $result = $this->model_files->upload_file('nid_img_front', './uploads/nid_imgs','jpg|jpeg|png' );

            if( $result['success'] === true  ){
                $data['nid_img_front'] =  $result['data']['file_name'];
            } else {
                // redirect( base_url() . 'site/set_purchase_data?error=file' );
            }
        }

        if( $_FILES['nid_img_back']['name'] != '' ){
            $result = $this->model_files->upload_file('nid_img_back', './uploads/nid_imgs','jpg|jpeg|png' );

            if( $result['success'] === true  ){
                $data['nid_img_back'] =  $result['data']['file_name'];
            } else {
                // redirect( base_url() . 'site/set_purchase_data?error' );
            }
        }

        return $data;

    }

    private function intent_upload_bill_file( $data ){
        // intents upload files
        if( $_FILES['bill_img']['name'] != '' ){
            $result = $this->model_files->upload_file('bill_img', './uploads/bill_imgs','jpg|jpeg|png' );

            if( $result['success'] === true  ){
                $data['bill_img'] =  $result['data']['file_name'];
            } else {
                // redirect( base_url() . 'site/set_purchase_data?error=file' );
            }
        }

        return $data;
    }

    public function _full_text($value, $row){
        return mb_strtoupper($value);
    }

    public function _uppercase_text($value, $row){
        return mb_strtoupper($value);
    }

    public function ajax_get_prize_req_data(){
        $r = $this->main->getFullPrizeRequestByPrizeApplicationId( $this->input->post('prize_application_id') );
        echo json_encode($r);
    }

    public function resend_beneficts( $prize_application_id ){

        $this->_resend_beneficts( $prize_application_id );
        $this->session->set_flashdata('flash_message', 'Se han reenviado los beneficios.');
        redirect('cms/full_request_report');
    }

    private function _resend_beneficts( $prize_application_id ){

        $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

        $prizes_view = array();

            // previous assignment codes variables
        $exclusion_codes_id = array();

        foreach( $appRequestDetail as $ar ){

            $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id, array('prize_date_availability' => 1));

            foreach( $prize_detail as $pd ){
                $prizes_view[] = $pd;

                if( $pd->prize_detail_has_list_of_codes == 'yes' ){

                        /*
                         * we need to CHECK if this request has a previous code assignmet in order
                         * to retrieve the assigned codes or give new ones
                         */

                        if( $this->main->hasCodesAssigned( $appRequestDetail[0]->purchase_data_id ) ){

                            $code = $this->main->retrievePreviousCodes( $appRequestDetail[0]->purchase_data_id, $exclusion_codes_id );
                            $exclusion_codes_id[] = $code->id;


                        } else {
                            $code = $this->main->pickUpCode( $pd->prize_detail_id, $appRequestDetail[0]->purchase_data_id );
                        }

                        $prizes_view[ count($prizes_view) -1 ]->code = $code->value;
                    }
                }

                // TODO: resta de inventario
                /*if( $appRequestDetail[0]->prize_status != 'aprobado' ){
                    $tv_id_associate= $this->main->getPackageTvbyId($ar->tv_id);
                    $value_rest= $tv_id_associate->quanty;
                    $update_quanty= $value_rest - 1;

                    $this->main->update_quanty_prize($ar->tv_id,$update_quanty);
                }*/


            }

            $data = array(
                'prizes_view' => $prizes_view
                );

            $mail_body = $this->load->view('emails/email-accepted', $data, true);

            $r = $this->model_mails->singleEmail( $appRequestDetail[0]->email, $mail_body, 'LG - masqueuntv.com Solicitud aprobada' , $this->main->getModEmail() );
        }

    // updates a benefict quanty based on codes association
    public function update_benefit_quanty( $prize_detail_id ){

        $this->main->updateBenefictQuanty( $prize_detail_id );

    }

    public function admin_review( $prize_application_id ){



        $purchaseData = $this->main->getPurchaseDataByPrizeAppId( $prize_application_id );

        $data = array(
            'application_info'     => $this->main->getPrizeApplicationDetail( $prize_application_id ),
            'encrypted_app_id'     => $this->uriencryption->encode($prize_application_id),
            'denial_options'       => $this->main->getDenialOptions(),
            'token'                => $this->uriencryption->encode( $prize_application_id ),
            'promotional_code_txt' => $purchaseData->promotional_code_txt,
            'in_prize'             => true,
            'full_request_report'  => true,
            'cms_mode'             => true
        );


        $this->load->view('cms/opener', $data);
        $this->load->view('emails/email-admin-review');
        $this->load->view('cms/closure');


    }

    public function _admin_approve_deny_request( $primary_key, $row ){

        $prize_application = $this->main->getPrizeApplicationById( $row->prize_application_id );

        if( $prize_application->status != 'aprobado' && $prize_application->status != 'entregado' ){
            return site_url( 'cms/admin_review/' . $row->prize_application_id );
        } else {
            return "javascript:alert('Esta solicitud ya ha sido aprobada, no es posible realizar su moderación.')";
        }
    }

    public function request_historic( $prize_application_id ){

        $historic = $this->main->getRequestHistory( $prize_application_id );

        $data = array(
            'in_prize'             => true,
            'full_request_report'  => true,
            'historic'             => $historic
        );

        $this->load->view('cms/opener', $data);
        $this->load->view('cms/prizes/request_historic');
        $this->load->view('cms/closure');
    }

    public function handle_request_status(){

        if( $this->input->post('sendApprovalMail') == 1 ){
            // send approval email

            $prize_application_id = $this->input->post('prize_application_id');
            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            $prizes_view = array();

            // previous assignment codes variables
            $exclusion_codes_id = array();

            foreach( $appRequestDetail as $ar ){

                $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id, array(
                        'prize_date_availability'       => 1,
                        'prize_bool_quantity_available' => 1
                    ));

                foreach( $prize_detail as $pd ){
                    $prizes_view[] = $pd;

                    if( $pd->prize_detail_has_list_of_codes == 'yes' ){

                        /*
                         * we need to CHECK if this request has a previous code assignmet in order
                         * to retrieve the assigned codes or give new ones
                         */

                        if( $this->main->hasCodesAssigned( $appRequestDetail[0]->purchase_data_id ) ){

                            $code = $this->main->retrievePreviousCodes( $appRequestDetail[0]->purchase_data_id, $exclusion_codes_id );
                            $exclusion_codes_id[] = $code->id;


                        } else {
                            $code = $this->main->pickUpCode( $pd->prize_detail_id, $appRequestDetail[0]->purchase_data_id );

                            if( $appRequestDetail[0]->prize_status != 'aprobado' ){
                                // code substraction
                                // code available quanty update
                                $this->main->updateBenefictQuanty( $pd->prize_detail_id );
                            }
                        }

                        $prizes_view[ count($prizes_view) -1 ]->code = $code->value;
                    } else {
                        if( $appRequestDetail[0]->prize_status != 'aprobado' ){
                            // substraction prize available quanty
                            $this->main->substractBenefitQuanty( $pd->prize_detail_id );
                        }
                    }
                }

                // TODO: resta de inventario
                /*if( $appRequestDetail[0]->prize_status != 'aprobado' ){
                    $tv_id_associate= $this->main->getPackageTvbyId($ar->tv_id);
                    $value_rest= $tv_id_associate->quanty;
                    $update_quanty= $value_rest - 1;

                    $this->main->update_quanty_prize($ar->tv_id,$update_quanty);
                }*/


            }

            $data = array(
                'prizes_view' => $prizes_view
                );

            $mail_body = $this->load->view('emails/email-accepted', $data, true);

            $r = $this->model_mails->singleEmail( $appRequestDetail[0]->email, $mail_body, 'LG - masqueuntv.com Solicitud aprobada' , $this->main->getModEmail() );
        }



        switch( $this->input->post('request_status') ){

            case 'email_confirmacion':
                $this->session->set_flashdata('flash_message', 'Se ha actualizado el estado de la solicitud a: Email de confirmación.');
                break;

            case 'en_revision':
                $this->session->set_flashdata('flash_message', 'Se ha actualizado el estado de la solicitud a: En revisión.');
                break;

            case 'aprobado':
                $this->session->set_flashdata('flash_message', 'Se ha actualizado el estado de la solicitud a: Aprobado.');
                break;
        }

        if( $this->input->post('request_status') != 'no_aprobado' ){
            $r = $this->main->setPrizeApplicationStatus( $this->input->post('prize_application_id'), $this->input->post('request_status') );
            redirect( site_url('cms/full_request_report') );
        } else {

            $prize_application_id = $this->input->post('prize_application_id');
            $token = $this->uriencryption->encode( $prize_application_id );


            if( $this->input->post('sendDenialMail') == 1 ){
                redirect( site_url('site/form_deny_request?token=' . $token . '&admin_mode'  ) );
            } else {
                redirect( site_url('site/form_deny_request?token=' . $token . '&admin_mode=set' . '&no_email=set' ));
            }

        }


    }
}