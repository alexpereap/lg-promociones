<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once  dirname(__FILE__) .  '/../../vendor/swiftmailer/lib/swift_required.php';

class Site extends CI_Controller {

    function __construct(){


        parent::__construct();

        /*if( $_SERVER['REMOTE_ADDR'] != '127.0.0.1' && $_SERVER['REMOTE_ADDR'] != '201.234.247.226' ){
            redirect('maintenance');
            return;
        }*/


    }

    public function index(){
        // index page
        /*krumo($this->session->all_userdata());
        $this->load->view('test/login');*/

        $this->session->sess_destroy();

        $data = array(
            'in_login'           => true,
            'slides'             => $this->main->getAvailableHomeSlides()
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/login');
        $this->load->view('site/footer');
    }

    // update purchase data
    public function update_purchase_data(){


        $user_id = $this->session->userdata('id');

        // USER UPDATE
        /*$code = $this->input->post('code');

        $code_id = $this->main->getCode($code);
        $code_id = $code_id->id;*/


        // watch out of user_id issue (session issues)
        if( $user_id == null || empty($user_id) ){

            redirect('site/register_issue');
            return;
        }

        // watch out post corruption --> caused by larger files above php.ini upload_max_filesize property value
        if( count($_POST) == 0 ){

            redirect('site/size_issue');
            return;
        }

        // nid override
        $nid = trim($this->input->post('nid'));

        if( empty($nid) ){
            $nid = $this->session->userdata('nid');
        }

        $data = array(
         'name'            => $this->input->post('name'),
         'lastname'        => $this->input->post('lastname'),
         'cellphone'       => $this->input->post('cellphone'),
         'phone'           => $this->input->post('phone'),
         'phone_area_code' => intval($this->input->post('area_code')),
         'email'           => $this->input->post('email'),
         'nid'             => $nid,
         'last_login'      => date('Y-m-d g:h:i'),
        );

        $data = $this->intent_upload_user_files( $data );


        $this->main->updateUser($user_id, $data);

        // PURCHASE DATA UPADATE

        $data = array(
            'fk_user_id'           => $user_id,
            'fk_sells_point_id'    => $this->input->post('sell_point_id'),
            'purchase_date'        => $this->input->post('purchase_date'),
            'promotional_code_txt' => $this->input->post('promotional_code'),
            'purchase_amount'      => $this->input->post('purchase_amount')
        );

        if(  $this->input->post('promotorId') != '' ){
            $data['fk_promotor_id'] = $this->input->post('promotorId');
        }

        if( $_FILES['bill_img']['name'] != '' ){
            $result = $this->model_files->upload_file('bill_img', './uploads/bill_imgs','jpg|jpeg|png' );

            if( $result['success'] === true  ){
                $data['bill_img'] =  $result['data']['file_name'];
            } else {
                redirect( base_url() . 'site/set_purchase_data?error=file' );
            }
        }

        $purchase_data_id = $this->input->post('purchase_data_id');


        $this->main->updatePurchaseData( $purchase_data_id, $data );

        // tvs purchase data RE assocc

        // firsts clean current data
        $this->main->deletePurchaseDataTvByPurchaseDataId( $purchase_data_id );

        // then inserts the new data
        $data = array();
        if( isset( $_POST['purchase_data_tv'] ) ){

            foreach( $_POST['purchase_data_tv'] as $t ){
                $data[] = array(
                    'fk_purchase_data_id' => $purchase_data_id,
                    'fk_tv_id'            => $t['tv_id'],
                    'serial_tv'           => $t['serial']
                );
            }

            if( count($data) > 0 ){
                $this->main->insertPurchaseDataTvs( $data );
            }

        }


        // admin mode auto approval action
        if( isset($_POST['admin_mode']) ){

            /*$token = $this->uriencryption->encode( trim($this->input->post('prize_application_id')) );
            redirect('site/accept_request?token=' . $token);*/
            redirect('site/update_data_success_admin');
            return;

        } else {

            $this->sendUserConfirmEmail( $this->input->post('prize_application_id') );
            // encripted user id
            $token = $this->uriencryption->encode($user_id);
            redirect('site/update_data_success?token=' . $token );
        }

    }

    public function save_data_success_admin(){

        $data = array(
            'message_title'     => '¡Registro satisfactorio!',
            'message_content'   => 'Se ha ingresado correctamente la información de esta solicitud. <a href="' . site_url() . 'cms/full_request_report" >Volver al cms</a>.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function update_data_success_admin(){
        $data = array(
            'message_title'     => '¡Actualización satisfactoria!',
            'message_content'   => 'Se ha actualzado correctamente la información de esta solicitud. <a href="' . site_url() . 'cms/full_request_report" >Volver al cms</a>.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function update_data_success(){

        $token = $this->input->get('token');

        $data = array(
            'message_title'     => '¡Actualización satisfactoria!',
            'message_content'   => 'Hemos envíado un email de confirmación de datos a tu correo electronico. Por favor revísalo para confirmar tu solicitud.
            <br>Haz click <a href="site/check_status?token=' . $token . '" >aquí</a> para revisar el estado de tu solicitud.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    // save purchase data on DB.
    public function save_purchase_data(){

        // watch out post corruption --> caused by larger files above php.ini upload_max_filesize property value
        if( count($_POST) == 0 ){
            redirect('site/size_issue');
            return;
        }

        $this->load->model("model_files");

        // if we're dealing with a new user, insert it on database
        if( $this->session->userdata('new_user') === true ){
            $user_id = $this->insert_user_data();

        } else {
            $user_id = $this->session->userdata('id');

            // optional email update
            if( $this->session->userdata('priority_email') != $this->session->userdata('email') && !empty($user_id) ){

                $this->main->updateUser( $user_id, array('email'  => $this->session->userdata('priority_email') ) );
            }
        }

        // watch out of user_id issue
        if( $user_id == null || empty($user_id) ){

            redirect('site/register_issue');
            return;
        }




        if( isset($_POST['code']) ){

            $code = $this->input->post('code');

            $code_id = $this->main->getCode($code);
            $code_id = $code_id->id;

        } else {

            // online buy case (the online user does not have a code)
            $code = '';
            $code_id = null;
        }

        /*krumo($user_id);
        krumo($_POST);
        krumo($_FILES);*/

        // Purchase data insert

        $data = array(
            'fk_user_id'           => $user_id,
            'fk_sells_point_id'    => $this->input->post('sell_point_id'),
            'purchase_date'        => $this->input->post('purchase_date'),
            'fk_codes_id'          => $code_id,
            'promotional_code_txt' => $this->input->post('promotional_code'),
            'code_txt'             => $code,
            'purchase_amount'      => $this->input->post('purchase_amount')
        );

        if( $this->input->post('promotorId') != '' ){
            $data['fk_promotor_id'] = $this->input->post('promotorId');
        }

        if( $_FILES['bill_img']['name'] != '' ){
            $result = $this->model_files->upload_file('bill_img', './uploads/bill_imgs','jpg|jpeg|png' );

            if( $result['success'] === true  ){
                $data['bill_img'] =  $result['data']['file_name'];
            } else {
                redirect( base_url() . 'site/set_purchase_data?error=file' );
            }
        }

        $purchase_data_id = $this->main->insertPurchaseData( $data );

        // tvs purchase data assocc

        $data = array();
        if( isset( $_POST['purchase_data_tv'] ) ){

            foreach( $_POST['purchase_data_tv'] as $t ){
                $data[] = array(
                    'fk_purchase_data_id' => $purchase_data_id,
                    'fk_tv_id'            => $t['tv_id'],
                    'serial_tv'           => $t['serial']
                );
            }

            if( count($data) > 0 ){
                $this->main->insertPurchaseDataTvs( $data );
            }

            // krumo($data);
        }

        // prize application insert

        $data = array(
            'fk_purchase_data_id' => $purchase_data_id,
        );

        // krumo($data);

        $prize_application_id = $this->main->insertPrizeApplication($data);

        if( isset($_POST['admin_mode']) ){
            $data['status'] = 'en_revision';
            $this->main->assocPromoCodeUser( $code_id, $user_id, $prize_application_id );
        }

        // Sends email confirm data to person if isn't admin
        if( isset($_POST['admin_mode']) ){
            redirect('site/save_data_success_admin');
            return;
        } else {
            $this->sendUserConfirmEmail( $prize_application_id );
            $token = $this->uriencryption->encode($user_id);
            redirect('site/request_success?token=' . $token );
        }


    }


    public function request_success(){

        $token = $this->input->get('token');

        $data = array(
            'message_title'     => '¡Confirmación Necesaria!',
            'message_content'   => '<h3>Hemos enviado un email de confirmación con tus datos a tu correo electrónico. Por favor revisalo para confirmar tu solicitud.
            <br>Haz click <a href="site/check_status?token=' . $token . '" >aquí</a> para revisar el estado de tu solicitud.</h3>'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function sendUserConfirmEmail( $prize_application_id ){



        $data = array(
            'application_info' => $this->main->getPrizeApplicationDetail( $prize_application_id ),
            'encrypted_app_id' => $this->uriencryption->encode($prize_application_id) // the token
        );

        $mail_body = $this->load->view('emails/email-register-confirm', $data, true);

        // SEND LOGIC
        $this->model_mails->singleEmail( $data['application_info'][0]->email, $mail_body, 'LG - masqueuntv.com Confirmación de datos' );

    }

    // inserts new user data
    private function insert_user_data(){
        $data = array(
            'name'            => $this->input->post('name'),
            'lastname'        => $this->input->post('lastname'),
            'cellphone'       => $this->input->post('cellphone'),
            'phone'           => $this->input->post('phone'),
            'phone_area_code' => intval($this->input->post('area_code')),
            'email'           => $this->input->post('email'),
            'nid'             => $this->input->post('nid'),
            'last_login'      => date('Y-m-d g:h:i')
        );


        $data = $this->intent_upload_user_files( $data );

        $this->session->set_userdata($data);

        $uid = $this->main->insertUser($data);
        $this->session->set_userdata('id', $uid);
        $this->session->set_userdata('new_user', false);

        return $uid;
    }

    private function intent_upload_user_files( $data ){

        // intents upload files
        if( $_FILES['nid_img_front']['name'] != '' ){
            $result = $this->model_files->upload_file('nid_img_front', './uploads/nid_imgs','jpg|jpeg|png' );

            if( $result['success'] === true  ){
                $data['nid_img_front'] =  $result['data']['file_name'];
            } else {
                // redirect( base_url() . 'site/set_purchase_data?error=file' );
            }
        }

        if( $_FILES['nid_img_back']['name'] != '' ){
            $result = $this->model_files->upload_file('nid_img_back', './uploads/nid_imgs','jpg|jpeg|png' );

            if( $result['success'] === true  ){
                $data['nid_img_back'] =  $result['data']['file_name'];
            } else {
                // redirect( base_url() . 'site/set_purchase_data?error' );
            }
        }

        return $data;

    }

    // public view for purchase data
    public function set_purchase_data(){
        $this->load->view('test/purchase_data');
    }

    public function register(){

        // clears user session data
        if( isset($_GET['admin_mode']) ){
            $this->session->unset_userdata('id');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('nid');
            $this->session->unset_userdata('name');
            $this->session->unset_userdata('lastname');
            $this->session->unset_userdata('cellphone');
            $this->session->unset_userdata('phone');
            $this->session->unset_userdata('phone_area_code');
            $this->session->unset_userdata('nid_img_front');
            $this->session->unset_userdata('nid_img_back');
            $this->session->unset_userdata('created_at');
            $this->session->unset_userdata('last_login');
            $this->session->unset_userdata('verified');
            $this->session->unset_userdata('loggedin_public');
            $this->session->unset_userdata('new_user');

            $this->session->set_userdata('new_user', true);
        }

        // redirects to login if there is no active session
        if( !isset($_GET['admin_mode']) )
        if( $this->session->userdata('loggedin_public') === false )
            redirect('/');

        $data = array(
            'cities'         => $this->main->getStoreAvailableCities(),
            'promo_code_txt' => $this->main->getPromoCodeText(),
            'user_purchase_data' => array()
        );


        if( $this->session->userdata('new_user') === false ){
            $user_id = $this->session->userdata('id');

            $data['user_purchase_data'] = $this->main->getPurchaseDataPrizeApplicationByUserId( $user_id );

            foreach( $data['user_purchase_data'] as $key_user_purchase_data => $pd ){

                $data['user_purchase_data'][$key_user_purchase_data]->prize_application_status_human = $this->prizeStatusHumanReadable($pd->prize_application_status);

                // gets the associated prize aplication id
                $prize_application = $this->main->getPrizeApplicationByPurchaseDataId( $pd->id );

                $appRequestDetail = $this->main->getPrizeApplicationDetail( $pd->prize_application_id );

                foreach( $appRequestDetail as $ar ){

                    // $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id , array('prize_date_availability' => 1));
                    $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id );

                    foreach( $prize_detail as $pd ){
                        $data['user_purchase_data'][$key_user_purchase_data]->prizes_detail[] = $pd;

                    }

                }

            }

        }

        $this->load->view('site/header', $data);
        $this->load->view('site/register');
        $this->load->view('site/footer');
    }

    private function prizeStatusHumanReadable($status){

        switch ($status){

            case 'email_confirmacion':
                return "Email confirmación";
                break;

            case 'en_revision':
                return "En revisión";
                break;

            case 'no_aprobado':
                return "No Aprobado";
                break;

            case 'aprobado':
                return "Aprobado";
                break;

            case 'entregado':
                return "Entregado";
                break;
        }


    }

    public function register_update(){


        // echo $this->uriencryption->encode(14);

        $prize_application_id = intval($this->uriencryption->decode( $this->input->get('token') ));

        if( is_int( $prize_application_id ) ){

            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            if( count( $appRequestDetail ) > 0 ){

               // if this is already aprobed take me out of here
               if( !isset($_GET['admin_mode']) )
               if( $appRequestDetail[0]->prize_status == 'aprobado' || $appRequestDetail[0]->prize_status == 'entregado'   ){
                    redirect('site/already_approved');
                    return;
                }

                if( !isset($_GET['admin_mode']) )
                if( $appRequestDetail[0]->prize_status == 'en_revision' ){
                    redirect('site/in_revision');
                    return;
                }

                $uid = $appRequestDetail[0]->user_id;
                $u = $this->main->getUserById($uid);

                $this->loginExistantUser($u);

                $angular_data = array();

                // preload angular data
                $angular_prizes = array();

                $purchase_data = $this->main->getPurchaseDataByPrizeAppId($prize_application_id);

                foreach( (array) $appRequestDetail as $ard ){

                    $r = $this->main->getAvailableTvPrize( $ard->tv_id );
                    $data_tv_purchase = $this->main->getPurchaseDataTv( $purchase_data->id, $ard->tv_id );

                    $beneficts_availability = $this->parseBenefictsAvailability( $ard->tv_id );

                     $angular_prizes[] = array(
                        'id'                        => $r->tv_id,
                        'tv_id'                     => $r->tv_id,
                        'label'                     => $r->reference,
                        'value'                     => $r->reference,
                        'title'                     => $r->prize_title,
                        'terms'                     => $r->prize_terms,
                        'image'                     => $r->prize_image,
                        'description'               => $r->prize_description,
                        'quanty'                    => $r->prize_quanty,
                        'date_availability'         => $beneficts_availability->date_availability,
                        'serial'                    => $data_tv_purchase->serial_tv,
                        'has_promo_code'            => filter_var($r->prize_has_promo_code, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
                        'beneficts_available_dates' => $beneficts_availability->beneficts_available_dates
                    );
                }

                $angular_data['prizes'] = $angular_prizes;
                $sellpoint = $this->main->getSellPointById( $purchase_data->fk_sells_point_id );


                $angular_data['stores'] = $this->main->getCityStores( $sellpoint->city_id );
                $angular_data['sellpoints'] = $this->main->getStoreSellPoints( $sellpoint->store_id );

                $data = array(
                    'cities'         => $this->main->getStoreAvailableCities(),
                    'promo_code_txt' => $this->main->getPromoCodeText(),
                    'edit_mode'      => true,
                    'angular_data'   => $angular_data,
                    'sellpoint'      => $sellpoint,
                    'purchase_data'  => $purchase_data,
                    'code'           => $this->main->getCodeById( $purchase_data->fk_codes_id ),
                    'promotor'       => $this->main->getPromotor($purchase_data->fk_promotor_id)
                );

                // prevents unset code if was an online store
                if( count($data['code']) == 0 ){
                    $data['code'] = (object) array('value' => null);
                }

                $this->load->view('site/header', $data);
                $this->load->view('site/register');
                $this->load->view('site/footer');

            } else{
                $this->token_error("No hemos podido encontrar esta solicitud, no existe o ya ha sido procesada.");
            }

        } else {

            $this->token_error("No hemos podido encontrar esta solicitud, no existe o ya ha sido procesada.");
        }
    }

    public function token_error($str){

        $data = array(
            'message_title'     => 'Hubo un problema',
            'message_content'   => $str
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    // check if user exists by nid
    public function user_exists($nid){

        if( empty($nid) ) {

            return array('result' => false);
        }
        $u = $this->main->getUserByNid($nid);

        count( $u ) > 0 ? $result['result'] = true : $result['result'] = false;
        return ($result);
    }

    // ajax user exists
    public function ajax_users_exists(){
        echo  json_encode($this->user_exists($_GET['nid']));
    }

    // stores basic login data for firstime user on session
    public function login(){

        if( $_SERVER['REQUEST_METHOD'] != "POST" ){
            die('bad request method');
            return;
        }

        $nid = $this->input->post('nid');

        // checks if is already registred
        $u = $this->main->getUserByNid($nid);
        if( count($u) > 0 ){

            $this->loginExistantUser($u);

        } else {

            $data = array(
                'nid'             => $this->input->post('nid'),
                'email'           => $this->input->post('email'),
                'loggedin_public' => true,
                'new_user'        => true
            );

            $this->session->set_userdata($data);
        }

        // opc email (for further changes of email)

        $this->session->set_userdata('priority_email', $this->input->post('email'));
        redirect('site/register');

    }

    /**
    * @param $u (user data array)
    */
    private function loginExistantUser( $u ){

        $this->session->set_userdata( (array)$u );


            $this->main->updateLastLogin($u->id);

            $this->session->set_userdata('loggedin_public', true);
            $this->session->set_userdata('new_user', false);
    }

    public function logout(){
        $this->session->sess_destroy();
    }

    // callback all neccesary info prior to prelogin
    public function pre_login($nid, $codeVal = null){

        if( !$codeVal )
            $codeVal = null;

        $u    = $this->main->getUserByNid($nid);
        $code = $this->main->getCode($codeVal);

        $code_available = false;

        if( count($code) > 0 ){
            $code_available = $code->available == 'si' ? true : false;
        }

        $result = array(
            'user_exists' => count($u) > 0 ? true : false,
            'code' => array(
                'value' => $codeVal,
                'exists' => count($code) > 0 ? true : false,
                'available' => $code_available,
            )
             // TODO : 'user_codes' => array[]
        );

        return $result;
    }

    // ajax info pre login
    public function ajax_pre_login(){
        echo json_encode ( $this->pre_login($this->input->get('nid'), $this->input->get('code')) );
    }

    // autocomplete refs tv
    public function get_available_prizes_tvs_byref(){
        $ref = $this->input->get('term');

        $tvs = array();

        if( strlen($ref) >= 2 ){

            $result = $this->main->getAvailablePrizesTvsByRef($ref);
            foreach( $result as $r ){

                $beneficts_availability = $this->parseBenefictsAvailability( $r->tv_id );

                $tvs[] = array(
                    'id'                        => $r->tv_id,
                    'tv_id'                     => $r->tv_id,
                    'label'                     => $r->reference,
                    'value'                     => $r->reference,
                    'title'                     => $r->prize_title,
                    'terms'                     => $r->prize_terms,
                    'image'                     => $r->prize_image,
                    'description'               => $r->prize_description,
                    'quanty'                    => $r->prize_quanty,
                    'date_availability'         => $beneficts_availability->date_availability,
                    'unavailable_message'       => $beneficts_availability->unavailable_message,
                    'has_promo_code'            => filter_var($r->prize_has_promo_code, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
                    'beneficts_available_dates' => $beneficts_availability->beneficts_available_dates
                );
            }

        }
        echo json_encode($tvs);
    }

    // gets prizes details - stores returns not available products by date or 0 quantity
    public function parseBenefictsAvailability($tv_id){

        $beneficts = $this->main->getTvBeneficts( $tv_id );

        $beneficts_count = count($beneficts);
        $unavailable_count = 0;
        $unavailable_products = array();
        $unavailable_products_ids = array();

        $beneficts_available_dates = array();

        foreach( $beneficts as $b ){

            // if not date available
            if( filter_var($b->prize_date_availability, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === false ){
                $unavailable_count++;
                $unavailable_products[] = $b->prize_detail_title;
                $unavailable_products_ids[] = $b->prize_detail_id;
            }

            // quantity availability
            if( filter_var($b->prize_bool_quantity_available, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === false && !in_array( $b->prize_detail_id, $unavailable_products_ids ) ){
                $unavailable_count++;
                $unavailable_products[] = $b->prize_detail_title;
                $unavailable_products_ids[] = $b->prize_detail_id;

            }

            $beneficts_available_dates[] = array(
                'available_from'  => $b->prize_available_from,
                'available_until' => $b->prize_available_until
            );

        }

        if( $unavailable_count == 0 ){
            $date_availability = TRUE; // all beneficts available
        } else if( $unavailable_count == $beneficts_count ){
            $date_availability = FALSE; // no beneficts available
        } else{
            $date_availability = 'partial'; // some beneficts available
        }

        $unavailable_message = "Los siguientes beneficios no están disponibles para esta referencia de TV: " . implode(', ',$unavailable_products);


        return (object) array(
            'date_availability'         => $date_availability,
            'unavailable_message'       => $unavailable_message,
            'beneficts_available_dates' => $beneficts_available_dates,
            'prize_available_quanty'
        );

    }

    public function get_user_assoc_tvs_qty($user_id){
        return $this->main->getUserAssocTvsQty($user_id);
    }

    public function ajax_get_user_assoc_tvs_qty(){
        echo json_encode( array('qty' => $this->main->getUserAssocTvsQty( $this->input->get('user_id') )) );
    }

    public function testEmail(){

        $transport = Swift_MailTransport::newInstance();

        $message = Swift_Message::newInstance()->setTo(array( 'apexmd21@gmail.com' ))
        ->setSubject("Mail de prueba");

        $message->setFrom("no-reply@wunderman.com", "Prueba");
        $mail_content = $this->load->view('emails/email-admin-review', null, true);

        $message->setBody($mail_content, 'text/html', 'UTF-8'  );

        $transport = Swift_MailTransport::newInstance();
        $mailer = Swift_Mailer::newInstance($transport);

        $mailer->send($message);
    }



    public function ajax_get_city_stores(){
        $result = $this->main->getCityStores( $this->input->get('city_id') );

        echo json_encode($result);
    }

    public function ajax_get_store_sell_points(){
        $result = $this->main->getStoreSellPoints( $this->input->get('store_id') );

        echo json_encode($result);
    }

    public function ajax_get_code(){
        $result = $this->main->getCode( $this->input->get('code') );

        if( count($result) > 0 ){
            echo json_encode(array(
                'success'   => true,
                'data'      => $result
            ));
        } else {
            echo json_encode(array(
                'success'   => false,
            ));
        }

    }

    public function confirm_data(){

        $prize_application_id = intval($this->uriencryption->decode( $this->input->get('token') ));

        if( is_int( $prize_application_id ) ){

            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            if( count( $appRequestDetail ) > 0 ){

                // if this is already aprobed take me out of here
                if( $appRequestDetail[0]->prize_status == 'aprobado' || $appRequestDetail[0]->prize_status == 'entregado' ){
                    redirect('site/already_approved');
                    return;
                }

                if( $appRequestDetail[0]->prize_status == 'en_revision' ){
                    redirect('site/in_revision');
                    return;
                }

                // if code id isn't empty we're dealing with a non online buy which has a code_id associated
                if( ! empty($appRequestDetail[0]->code_id) ){

                    $this->main->assocPromoCodeUser( $appRequestDetail[0]->code_id, $appRequestDetail[0]->user_id, $prize_application_id );
                }

                $this->main->updatePrizeApplicationStatus( $prize_application_id, 'en_revision' );
                $this->sendAdminReviewEmail( $prize_application_id );

                $user_id = $appRequestDetail[0]->user_id;

                $this->main->verifyUser( $user_id );

                $token = $this->uriencryption->encode($user_id);

                redirect('site/confirm_data_success?token=' . $token );

            } else {
                $this->token_error("No hemos podido encontrar esta solicitud, no existe o ya ha sido procesada.");
            }
        } else {
            $this->token_error("No hemos podido encontrar esta solicitud, no existe o ya ha sido procesada.");
        }
    }

    public function sendAdminReviewEmail( $prize_application_id ){



        $purchaseData = $this->main->getPurchaseDataByPrizeAppId( $prize_application_id );

        $data = array(
            'application_info' => $this->main->getPrizeApplicationDetail( $prize_application_id ),
            'encrypted_app_id' => $this->uriencryption->encode($prize_application_id),
            'denial_options'   => $this->main->getDenialOptions(),
            'token'            => $this->uriencryption->encode( $prize_application_id ),
            'promotional_code_txt' => $purchaseData->promotional_code_txt
        );


        $mail_body = $this->load->view('emails/email-admin-review', $data, true);

        // TODO: SEND LOGIC
        $this->model_mails->singleEmail( $this->main->getModEmail(), $mail_body, 'LG - masqueuntv.com Moderación de usuario' );

    }

    public function deny_request(){



        $prize_application_id = intval($this->uriencryption->decode( $this->input->get('token') ));

        if( is_int( $prize_application_id ) ){

            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            if( count( $appRequestDetail ) > 0 ){

                $this->input->get('denial_option') == 1 ? $denial = null : $denial = $this->main->getDenialOptionById( $this->input->get('denial_option') );

                $data = array(
                    'denial'           => $denial,
                    'application_info' => $appRequestDetail,
                    'token'            => $this->uriencryption->encode( $prize_application_id ),
                    'observation'      => $this->input->get('observation')
                );

                $mail_body = $this->load->view('emails/email-declined', $data, true);
                $this->main->updatePrizeApplicationStatus( $prize_application_id, 'no_aprobado', $data['observation'] );

                if( !isset( $_GET['no_email'] ) ){
                    $this->model_mails->singleEmail( $appRequestDetail[0]->email, $mail_body, 'LG - masqueuntv.com Solicitud de beneficio rechazada' );
                }

                $uri = 'site/deny_success';

                if( isset($_GET['admin_mode']) )
                    $uri .= '?admin_mode';

                redirect( $uri );

            } else {
                $this->token_error("token invalido");
            }
        } else {
            $this->token_error("token invalido");
        }
    }

    public function form_deny_request(){

        $data = array(
            'denial_options'   => $this->main->getDenialOptions(),
            'token'            => $this->input->get('token')
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/deny_request_form');
        $this->load->view('site/footer');
    }

    public function accept_request(){

        $prize_application_id = intval($this->uriencryption->decode( $this->input->get('token') ));

        if( is_int( $prize_application_id ) ){

            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            if( count( $appRequestDetail ) > 0 ){


                // if this is already aprobed take me out of here
               if( $appRequestDetail[0]->prize_status == 'aprobado' || $appRequestDetail[0]->prize_status == 'entregado' ){
                    redirect('site/already_approved');
                    return;
               }

               $prizes_view = array();

               foreach( $appRequestDetail as $ar ){

                    $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id, array(
                        'prize_date_availability'       => 1,
                        'prize_bool_quantity_available' => 1
                    ));

                    foreach( $prize_detail as $pd ){
                        $prizes_view[] = $pd;

                        // picks up a code - i.e netflix code
                        if( $pd->prize_detail_has_list_of_codes == 'yes' ){

                            $code = $this->main->pickUpCode( $pd->prize_detail_id, $appRequestDetail[0]->purchase_data_id );
                            $prizes_view[ count($prizes_view) -1 ]->code = $code->value;

                            // code available quanty update
                            $this->main->updateBenefictQuanty( $pd->prize_detail_id );
                        } else {
                            // substraction prize available quanty
                            $this->main->substractBenefitQuanty( $pd->prize_detail_id );
                        }

                    }

                    // TODO: resta de inventario

                    /*$tv_id_associate= $this->main->getPackageTvbyId($ar->tv_id);
                    $value_rest= $tv_id_associate->quanty;
                    $update_quanty= $value_rest - 1;

                    $this->main->update_quanty_prize($ar->tv_id,$update_quanty);*/

               }

               // associates promo code with user if necessary
               $this->main->assocPromoCodeUser( $appRequestDetail[0]->code_id, $appRequestDetail[0]->user_id, $prize_application_id );

               $data = array(
                    'prizes_view' => $prizes_view
               );

               $mail_body = $this->load->view('emails/email-accepted', $data, true);

               $this->model_mails->singleEmail( $appRequestDetail[0]->email, $mail_body, 'LG - masqueuntv.com Solicitud aprobada' );
               $this->main->updatePrizeApplicationStatus( $prize_application_id, 'aprobado' );

               redirect('site/approved_success');


            } else {
                $this->token_error("token invalido");
            }
        } else {
            $this->token_error("token invalido");
        }
    }

    public function confirm_data_success(){

        $token = $this->input->get('token');

        $data = array(
            'message_title'     => '¡Has confirmado tus datos!',
            'message_content'   => 'Hemos envíado tu solicitud para que sea evaluada por un asesor, pronto nos pondremos en contacto contigo.
            <br>Haz click <a href="site/check_status?token=' . $token . '" >aquí</a> para revisar el estado de tu solicitud.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function deny_success(){

        $data = array(
            'message_title'     => '¡Se ha rechazado la solicitud!',
            'message_content'   => 'Se ha enviado un email al cliente con las razones especificas del rechazo de su solicitud.'
        );

        if( isset($_GET['admin_mode']) ){
            $data['message_content'] .= ' <a href="' . site_url() . 'cms/full_request_report" >Volver al cms</a>.';
        }

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function already_approved(){

        $data = array(
            'message_title'     => 'Solicitud aprobada',
            'message_content'   => 'Esta solicitud ya ha sido aprobada'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function in_revision(){

        $data = array(
            'message_title'     => 'Estamos revisando tu solicitud',
            'message_content'   => 'Esta solicitud se encuentra en revisión, pronto un asesor de LG se colocará en contacto contigo.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function approved_success(){

        $data = array(
            'message_title'     => '¡Se ha aprobado la solicitud!',
            'message_content'   => 'Se ha enviado un email al cliente con los detalles de la aprobación de la solicitud.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function test(){
        $this->main->getModEmail();
    }

    public function check_status(){

        $uid = $this->uriencryption->decode( $this->input->get('token') );
        $u = $this->main->getUserById($uid);

        if( count($u) > 0 ){
            $this->loginExistantUser($u);

            redirect('site/register?action=openBillsModal');

        } else {
            die("Lo sentimos no hemos encontrado este usuario");
        }

    }

    public function register_issue(){

        $data = array(
            'message_title'     => 'Hubo un problema',
            'message_content'   => 'Detectamos un problema al procesar tus datos, puede que se trate de un problema con tu conexión de internet, por favor intentalo de nuevo haciendo click <a href="site/" >aquí</a>'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function size_issue(){

        $data = array(
            'message_title'     => 'Hubo un problema',
            'message_content'   => 'Lo sentimos pero el peso de las imágenes excede el límite permitido. Asegúrate de no superar los 6 MB por archivo, por favor intentalo de nuevo haciendo click <a href="site/register" >aquí</a>'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function php_info(){
        phpinfo();
    }

    public function get_promotor(){

        $promotor = $this->input->get('term');
        $promotors = array();

        if( strlen($promotor) >= 2 ){

            $result = $this->main->getPromotors( $promotor );
            foreach( $result as $r ){
                $promotors[] = array(
                    'id'    => $r->id,
                    'label' => $r->name . ' ' . $r->lastname,
                    'value' => $r->name . ' ' . $r->lastname
                );
            }
        }

        echo json_encode($promotors);
    }

    public function ajax_get_city_info(){
        $city_id = $this->input->get('city_id');

        if( !empty($city_id) ){

            $city = $this->main->getCityById($city_id);
            echo json_encode( $city );

        }
    }

    public function update_personal_data(){

        $user_id = $this->session->userdata('id');

        $data = array(
         'name'            => $this->input->post('name'),
         'lastname'        => $this->input->post('lastname'),
         'cellphone'       => $this->input->post('cellphone'),
         'phone'           => $this->input->post('phone'),
         'phone_area_code' => intval($this->input->post('area_code')),
         'email'           => $this->session->userdata('email'),
         'nid'             => $this->input->post('nid'),
         'last_login'      => date('Y-m-d g:h:i')
        );

        $data = $this->intent_upload_user_files( $data );
        $this->main->updateUser($user_id, $data);

        // updates session data
        $u = $this->main->getUserById($user_id);
        $this->session->set_userdata( (array)$u );

        // redirects previous url
        redirect($this->input->post('referer_url'));
    }

    public function ajax_verify_nid(){

        $result = array(
            'result' => false
        );

        $nid = $this->input->post('nid');
        $u = $this->main->getUserByNid( $nid );

        if( count($u) > 0 ){
            $result['result'] = true;
        }

        echo json_encode( $result );
    }

}

