<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Model{

    public function getTvBeneficts($tv_id){
        $q = $this->db->get_where('vw_prize_details_of_tv', array('tv_id' => $tv_id));
        return $q->result();
    }

    public function deletePrizePackage($id){
        $this->db->where('id', $id);
        $this->db->delete('tbl_prize');
    }

    // updates verified user status
    public function verifyUser( $user_id ){

        $this->db->where('id', $user_id);
        $this->db->update('tbl_user', array('verified' => 'si'));

    }

    // gets the moderator email address
    public function getModEmail(){

        $q = $this->db->get('tbl_mod_email');
        $r = $q->row();

        return $r->value;

    }

    public function getPrizeApplicationByPurchaseDataId( $purchase_data_id ){

        $this->db->join('tbl_prize_application pa', 'pa.fk_purchase_data_id = pd.id');
        $this->db->select('pa.*, pd.bill_number,');
        $q = $this->db->get_where('tbl_purchase_data pd', array('pd.id' => $purchase_data_id) );

        return $q->row();
    }

    public function getPurchaseDataPrizeApplicationByUserId( $user_id ){
        $this->db->order_by('created_at',' DESC');
        $q = $this->db->get_where('vw_purchase_data_prize_application', array('fk_user_id' => $user_id));
        return $q->result();
    }

    public function getAvailableHomeSlides(){
        $q = $this->db->get('vw_available_home_slides');
        return $q->result();
    }

    public function pickUpCode( $prize_detail_id, $purchase_data_id = false ){

        $this->db->order_by('id', 'random');
        $this->db->where('available', 'yes');
        $q = $this->db->get_where('tbl_prize_codes', array('fk_prize_detail_id' => $prize_detail_id));
        $r = $q->row();


        // disables the code
        $this->db->update('tbl_prize_codes', array('available' => 'no'), array('id' => $r->id));

        // makes purchase request and used code relation
        if( $purchase_data_id ){

            $this->db->insert('tbl_purchase_data_code_id', array(
                'fk_purchase_data_id' => $purchase_data_id,
                'fk_prize_codes_id'   => $r->id
            ));
        }

        return $r;

    }

    public function getPrizeDetailsByTvId( $tv_id, $constraints = array() ){

        $constraints['tv_id'] = $tv_id;
        $q = $this->db->get_where('vw_prize_details_of_tv', $constraints );
        return $q->result();
    }

    public function getDenialOptionById( $option_id ){

        $q = $this->db->get_where('tbl_denial_options', array('id' => $option_id));
        $r = $q->row();

        return $r->value;
    }

    public function getDenialOptions(){
        $q = $this->db->get('tbl_denial_options');
        return $q->result();
    }

    public function updatePrizeApplicationStatus( $id, $status, $observation = false ){
        $this->db->where('id', $id);
        $this->db->update('tbl_prize_application', array('status' => $status));


        // adds denial observation to historic
        if( $status == 'no_aprobado' && $observation ){
            $historic = $this->getLastHistoricEntry( $id );

            $this->db->where('id', $historic->id );
            $this->db->update('tbl_prize_application_historic', array('detail' => $observation) );
        }
    }

    public function deletePurchaseDataTvByPurchaseDataId( $purchase_data_id ){
        $this->db->where('fk_purchase_data_id', $purchase_data_id);
        $this->db->delete('tbl_purchase_data_tv');
    }

    public function updatePurchaseData( $purchase_data_id, $data ){
        $this->db->where('id', $purchase_data_id);
        $this->db->update('tbl_purchase_data', $data);
    }


    public function getPurchaseDataTv( $purchase_data_id, $tv_id ){
        $q = $this->db->get_where('tbl_purchase_data_tv',array('fk_purchase_data_id' => $purchase_data_id, 'fk_tv_id' => $tv_id));
        return $q->row();
    }

    public function getPurchaseDataByPrizeAppId( $prize_application_id ){

        $this->db->select('pa.id as prize_application_id, pa.created_at as prize_application_created_at, pd.*');
        $this->db->where('pa.id', $prize_application_id);
        $this->db->join('tbl_purchase_data pd', 'pa.fk_purchase_data_id = pd.id');
        $q = $this->db->get('tbl_prize_application pa');

        return $q->row();

    }

    public function getAvailableTvPrize($tv_id){
        $q = $this->db->get_where('vw_available_tv_prizes', array('tv_id' => $tv_id));
        return $q->row();

    }

    public function getPromoCodeText(){
        $q = $this->db->get_where('tbl_txt_promocode', array('id' => 1));
        $r = $q->row();

        return $r->value;

    }

    public function deletePrizeElements( $prize_id ){
        $this->db->where('fk_prize_id', $prize_id);
        $this->db->delete('tbl_prize_elements');
    }

    // updates prize package data
    public function updatePrizePackage($prize_id, $data){
        $this->db->where('id', $prize_id);
        $this->db->update('tbl_prize', $data);
    }

    // gets prize sellpoints
    public function getPrizePackageSellPoints( $prize_id ){
        $q = $this->db->get_where('tbl_prize_store', array('fk_prize_id' => $prize_id));
        return $q->result();
    }

    // gets the prizes of a prize package
    public function getPrizeElements($prize_id){
        $q = $this->db->get_where('tbl_prize_elements', array('fk_prize_id' => $prize_id));
        return $q->result();
    }

    public function getPrizePackageById( $prize_id ){
        $q = $this->db->get_where('tbl_prize' , array('id' => $prize_id) );
        return $q->row();
    }

    public function savePrizeElements($data){
        $this->db->insert_batch('tbl_prize_elements', $data);
    }

    public function savePrizePackageData( $data ){
        $this->db->insert('tbl_prize', $data);

        return $this->db->insert_id();
    }

    // build and returns city - sellpoints multiassoc array
    public function getCitySellpointsStoreAssoc(){

        $cities = $this->main->getStoreAvailableCities();

        foreach( $cities as $key => $c ){
            $cities[$key]->stores = $this->getCityStores( $c->id );

            foreach( $cities[$key]->stores as $key_s => $s ){
                $cities[$key]->stores[$key_s]->sellpoints = $this->getStoreSellPoints( $s->id );
            }
        }

        return $cities;
    }

    // get store sellpoints
    public function getStoreSellPoints( $store_id ){
        $q = $this->db->get_where('tbl_sells_point', array('fk_store_id' => $store_id));
        return $q->result();
    }

    // gets the prize items
    public function getPrizeItems(){
        $q = $this->db->get('tbl_prize_detail');
        return $q->result();
    }

    // deletes all prize store relationship of a prize
    public function deletePrizeStores($prize_id){
        $this->db->where('fk_prize_id', $prize_id);
        $this->db->delete('tbl_prize_store');
    }

    // inserts prize store relationship
    public function insertPrizeStores( $data ){
        $this->db->insert_batch('tbl_prize_store',$data);
    }

    // updates prize application status
    public function setPrizeApplicationStatus( $prize_application_id, $status ){

        $this->db->where('id', $prize_application_id);
        $r = $this->db->update('tbl_prize_application', array('status' => trim($status)));
        $this->db->last_query();
        return $r;
    }

    // gets prize application detail
    public function getPrizeApplicationDetail($prize_application_id){
        $q = $this->db->get_where( 'vw_prize_application_detail', array('prize_application_id' => $prize_application_id));
        return $q->result();
    }


    // gets the quanty of tvs associated with an user
    public function getUserAssocTvsQty($user_id){
        $q = $this->db->get_where('vw_user_associated_tvs_qty', array('user_id' => $user_id));
        $r = $q->row();

        if( count($r) > 0  )
            return $r->quanty;
        else
            return 'error';
    }

    public function insertPrizeApplication($data){
        $r = $this->db->insert('tbl_prize_application', $data);

        if( $r )
            return $this->db->insert_id();
        else
            return false;
    }

    public function assocCodeWithUser($data){
        $r = $this->db->insert('tbl_user_codes', $data );

        if( $r ){

            $insert_id = $this->db->insert_id();

            // drops trigger usage, manual code availability switch
            $this->db->where('id', $data['fk_codes_id']);
            $this->db->update('tbl_codes', array('available' => 'no'));

            return $insert_id;
        }
        else
            return false;
    }

    // assocciates tvs and purchase data
    public function insertPurchaseDataTvs($data){
        $this->db->insert_batch('tbl_purchase_data_tv', $data);
    }

    public function getAvailablePrizesTvsByRef($ref){
        $this->db->like('reference', $ref);
        $q = $this->db->get('vw_available_tv_prizes');

        return $q->result();
    }

    public function getTvsBasedOnRef($ref){
        $this->db->like('reference', $ref);
        $q = $this->db->get('tbl_tv');

        return $q->result();
    }

    public function insertPurchaseData($data){
        $r = $this->db->insert('tbl_purchase_data', $data);

        if( $r )
            return $this->db->insert_id();
        else
            return false;
    }

    public function getCodeById( $code_id ){
        $q = $this->db->get_where('tbl_codes', array('id' => $code_id));
        return $q->row();
    }

    // get code ... by code
    public function getCode($code){
        $q = $this->db->query("SELECT * FROM tbl_codes WHERE LOWER(value) = LOWER('$code')");
        return $q->row();
    }

    // updates last login date
    public function updateLastLogin($id){
        $this->db->where('id', $id);
        $this->db->update( 'tbl_user', array( 'last_login' => date('Y-m-d g:h:i')  ));
    }

    // updates user info
    public function updateUser($id, $data){
        $this->db->where('id', $id);
        $this->db->update('tbl_user', $data);
    }

    // inserts user data
    public function insertUser( $data ){
        $r = $this->db->insert('tbl_user', $data);

        if( $r )
            return $this->db->insert_id();
        else
            return false;
    }

    public function getUserById($uid){
        $q = $this->db->get_where('tbl_user', array('id' => $uid));
        return $q->row();
    }

    // get user by nid
    public function getUserByNid($nid){
        $q = $this->db->get_where('tbl_user', array('nid' => $nid));
        return $q->row();
    }

    // deletes sellpoint
    public function deleteSellpoint( $sellpoint_id ){
        $this->db->where('id', $sellpoint_id);
        return $this->db->delete('tbl_sells_point');
    }

    // updates sellpoint data
    public function updateSellpoint( $sellpoint_id, $data ){
        $this->db->where('id', $sellpoint_id);
        $this->db->update('tbl_sells_point', $data );
    }

    // get sellpoint byid
    public function getSellPointById( $sellpoint_id ){
        $q = $this->db->get_where('vw_sells_points', array( 'sellpoint_id' => $sellpoint_id ));
        return $q->row();
    }

    // save sell point data
    public function saveSellpoint($data){
        $this->db->insert('tbl_sells_point', $data);
    }

    // get the cities with sellpoints
    public function getStoreAvailableCities(){


        $q = $this->db->query("SELECT * FROM vw_cities_with_stores ORDER BY FIELD( name, 'Online' ) DESC , name");
        return $q->result();
    }

    // get stores (cadenas) of a city
    public function getCityStores( $city_id ){
        $q = $this->db->get_where('tbl_store', array('fk_city_id' => $city_id));
        return $q->result();
    }

    // get the full list of cities
    public function getCities(){
        $q = $this->db->get('tbl_city');
        return $q->result();
    }

    public function logInCmsUser( $userdata ){


        $user = $this->getCmsUserByUsername( $userdata['username'] );

        if( count( $user ) > 0 ){

            $password = $this->encrypt->decode( $user->password );

            if( $password == $userdata['password'] ){


                $session_data = array(
                    'user_id'   => $user->id,
                    'username'  => $user->username,
                    'logged_in' => TRUE
                    );

                $this->session->set_userdata($session_data);

                return true;
            }
        }

        return false;

    }

    public function getCmsUserByUsername( $username ){
        $q = $this->db->get_where('tbl_cms_user',  array('username' => $username));
        return $q->row();
    }

    public function get_available_tv_prizes()
    {
        $q = $this->db->query("SELECT t.* FROM tbl_tv t LEFT JOIN tbl_prize p ON p.fk_tv_id = t.id WHERE p.fk_tv_id is null;");
        return $q->result();
    }

    public function get_available_tv_prizes_update( $prize_id ){
        $q = $this->db->query("SELECT t.* , p.id as prize_id FROM tbl_tv t LEFT JOIN tbl_prize p ON p.fk_tv_id = t.id
            WHERE p.fk_tv_id is null OR p.id = $prize_id");

        return $q->result();
    }

    public function getPackageTvbyId($tv_id)
    {
       $q= $this->db->get_where('tbl_prize', array('fk_tv_id'=> $tv_id));
       return $q->row();
    }

    public function update_quanty_prize($tv_id,$update_quanty)
    {
        $data = array(
               'quanty' => $update_quanty,
               );
        $this->db->where('fk_tv_id', $tv_id);
        $this->db->update('tbl_prize', $data);
    }

    // checks if a code is associated with a user
    public function isCodeAssocWithUser($code_id){
        $q = $this->db->get_where('tbl_user_codes', array('fk_codes_id' => $code_id));

        if( $q->num_rows() > 0 )
            return true;

        return false;
    }

    // deletes all prize applications with the same promotional code
    public function cleanPrizeApplication( $prize_application_id ){

        $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

        $this->db->where( 'id !=', $appRequestDetail[0]->purchase_data_id );
        $this->db->where( 'fk_codes_id', $appRequestDetail[0]->code_id  );

        $this->db->delete('tbl_purchase_data');

    }

    public function getPromotors($promotor){

        $this->db->like('name', $promotor);
        $this->db->order_by('name','ASC');
        $q = $this->db->get('tbl_promotor');

        return $q->result();

    }

    public function getPromotor($id){
        $q = $this->db->get_where('tbl_promotor', array('id' => $id));
        return $q->row();
    }

    // checks if a purchase request has previous code assigned
    public function hasCodesAssigned( $purchase_data_id ){

        $q = $this->db->get_where('tbl_purchase_data_code_id', array('fk_purchase_data_id' => $purchase_data_id) );

        if( $q->num_rows() > 0 )
            return true;

        return false;
    }

    // gets a purchase request codes based in assign_date order and exclusion of different codes ids
    public function retrievePreviousCodes( $purchase_data_id, $exclusion_codes_id = array() ){

        // exclude already picked up codes
        if( count( $exclusion_codes_id ) > 0 ){
            foreach( $exclusion_codes_id as $cid ){
                $this->db->where('pdc.fk_prize_codes_id !=', $cid );
            }
        }

        // get one code
        $this->db->where('pdc.fk_purchase_data_id', $purchase_data_id);
        $this->db->order_by('pdc.assign_date','ASC');

        $this->db->join('tbl_prize_codes pc', 'pc.id = pdc.fk_prize_codes_id');
        $this->db->select('pc.*');

        $q = $this->db->get('tbl_purchase_data_code_id pdc');
        return $q->row();
    }

    public function getPrizeCodeById( $code_id ){

        $q = $this->db->get_where('tbl_prize_codes', array('id' => $code_id) );
        return $q->row();
    }

    public function getCityById( $city_id ){

        $q = $this->db->get_where('tbl_city', array('id' => $city_id) );
        return $q->row();
    }

    public function getTvs(){
        $q = $this->db->get('tbl_tv');
        return $q->result();
    }

    public function deletePurchaseDataByPrizeApplicationId( $prize_application_id ){
        $query = "DELETE FROM tbl_purchase_data
            WHERE id IN (

            SELECT fk_purchase_data_id
            FROM tbl_prize_application
            WHERE id = $prize_application_id

            )";

        $this->db->query($query);
    }

    public function getFullPrizeRequestByPrizeApplicationId( $prize_application_id ){

        $q = $this->db->get_where('vw_full_prize_requests', array('prize_application_id' => $prize_application_id));
        return $q->row();
    }

    // updates benefict quanty for benefits with code association
    public function updateBenefictQuanty( $prize_detail_id ){
        $query = "UPDATE tbl_prize_detail as tpd

            INNER JOIN(

                select count(pc.id) as num_codes, pc.fk_prize_detail_id as prize_detail_id from tbl_prize_detail pd
                join tbl_prize_codes pc on pc.fk_prize_detail_id = pd.id
                where pd.id = $prize_detail_id and pc.available = 'yes'

            ) as U ON tpd.id = U.prize_detail_id

            SET tpd.available_quanty = U.num_codes";

        $this->db->query($query);
    }

    // substract 1 unit to a benefit
    public function substractBenefitQuanty( $prize_detail_id ){

        $query = "UPDATE tbl_prize_detail SET  available_quanty = (available_quanty - 1) WHERE id = $prize_detail_id";
        $this->db->query($query);
    }

    // assocs lg code with user
    public function assocPromoCodeUser( $code_id, $user_id, $prize_application_id ){
        // cleans requests with same promotional code
        $this->cleanPrizeApplication( $prize_application_id );

        // if the code isn't associated with an user ...
        if( $this->isCodeAssocWithUser( $code_id ) === false ){

            $data = array(
                'fk_user_id'  => $user_id,
                'fk_codes_id' => $code_id
            );

            $this->assocCodeWithUser($data);
        }
    }

    public function getPrizeApplicationById( $prize_application_id ){

        $q = $this->db->get_where('tbl_prize_application', array('id' => $prize_application_id));
        return $q->row();
    }

    public function getRequestHistory( $prize_application_id ){
        $this->db->order_by( 'date ASC' );
        $q = $this->db->get_where('tbl_prize_application_historic', array('fk_prize_application_id' => $prize_application_id));
        return $q->result();
    }

    // gets last historic entry for a prize application
    public function getLastHistoricEntry( $prize_application_id ){
        $this->db->order_by( 'id DESC' );
        $this->db->limit(1);
        $q = $this->db->get_where('tbl_prize_application_historic', array('fk_prize_application_id' => $prize_application_id));

        return $q->row();
    }


}