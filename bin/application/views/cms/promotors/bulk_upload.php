<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="cms/parse_promotor_bulk_upload" >
<fieldset>

<!-- Form Name -->
<legend>Subir archivo de promotores</legend>

<!-- File Button -->
<div class="form-group">
<?php if( isset($_GET['success']) ): ?>
    <div class="alert alert-success" role="alert">Se ha completado la operación, revisa el listado de promotores <a href="cms/promotor">aquí</a></div>
<?php endif; ?>

<?php if( isset($_GET['error']) ): ?>
    <div class="alert alert-danger" role="alert"><?php echo $this->input->get('error') ?></div>
<?php endif; ?>
  <label class="col-md-4 control-label" for="filebutton">Archivo (xls, xlsx)</label>
  <div class="col-md-4">
    <input id="promotorFile" name="promotorFile" class="input-file" type="file">
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="save"></label>
  <div class="col-md-8">
    <button id="save" name="save" class="btn btn-success">Guardar</button>
  </div>
</div>

</fieldset>
</form>
