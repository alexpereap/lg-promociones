<?php if( $this->session->flashdata('flash_message') ): ?>
    <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('flash_message'); ?></div>
<?php endif; ?>

<div id="fullRequestFilters">
    <form action="cms/set_request_filters" method="POST" >
        <h4>Campos</h4>
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <tr>
                        <td> <label><input type="checkbox" name="filters[name]" <?php if (isset($this->session->userdata('full_request_report')['name'])) echo 'checked' ?> value="1">Nombre/s </label> </td>
                        <td> <label><input type="checkbox" name="filters[lastname]" <?php if (isset($this->session->userdata('full_request_report')['lastname'])) echo 'checked' ?> value="1">Apellido/s </label> </td>
                        <td> <label><input type="checkbox" name="filters[email]" <?php if (isset($this->session->userdata('full_request_report')['email'])) echo 'checked' ?> value="1">Email </label> </td>
                        <td> <label><input type="checkbox" name="filters[cellphone]" <?php if (isset($this->session->userdata('full_request_report')['cellphone'])) echo 'checked' ?> value="1">Celular </label> </td>
                        <td> <label><input type="checkbox" name="filters[phone]" <?php if (isset($this->session->userdata('full_request_report')['phone'])) echo 'checked' ?> value="1">Telefono </label> </td>
                        <td> <label><input type="checkbox" name="filters[nid]" <?php if (isset($this->session->userdata('full_request_report')['nid'])) echo 'checked' ?> value="1">No. cédula </label> </td>
                    </tr>
                    <tr>

                        <td> <label><input type="checkbox" name="filters[nid_img_front]" <?php if (isset($this->session->userdata('full_request_report')['nid_img_front'])) echo 'checked' ?> value="1">Imagen cédula frente</label> </td>
                        <td> <label><input type="checkbox" name="filters[nid_img_back]" <?php if (isset($this->session->userdata('full_request_report')['nid_img_back'])) echo 'checked' ?> value="1">Imagen cédula atras</label> </td>
                        <td> <label><input type="checkbox" name="filters[prize_request_qty]" <?php if (isset($this->session->userdata('full_request_report')['prize_request_qty'])) echo 'checked' ?> value="1">Cantidad de tvs comprados</label> </td>
                        <td> <label><input type="checkbox" name="filters[sellpoint_name]" <?php if (isset($this->session->userdata('full_request_report')['sellpoint_name'])) echo 'checked' ?> value="1">Punto de venta </label> </td>
                        <td> <label><input type="checkbox" name="filters[store_name]" <?php if (isset($this->session->userdata('full_request_report')['store_name'])) echo 'checked' ?> value="1">Cadena </label> </td>
                        <td> <label><input type="checkbox" name="filters[city_name]" <?php if (isset($this->session->userdata('full_request_report')['city_name'])) echo 'checked' ?> value="1">Ciudad</label> </td>
                    </tr>
                    <tr>

                        <td> <label><input type="checkbox" name="filters[tv_references]" <?php if (isset($this->session->userdata('full_request_report')['tv_references'])) echo 'checked' ?>  value="1">Referencia tv / serial </label> </td>
                        <td> <label><input type="checkbox" name="filters[tv_references_list]" <?php if (isset($this->session->userdata('full_request_report')['tv_references_list'])) echo 'checked' ?>  value="1">Referencia/s</label> </td>
                        <td> <label><input type="checkbox" name="filters[tv_serials_list]" <?php if (isset($this->session->userdata('full_request_report')['tv_serials_list'])) echo 'checked' ?>  value="1">Serial/es</label> </td>
                        <td> <label><input type="checkbox" name="filters[bill_img]" <?php if (isset($this->session->userdata('full_request_report')['bill_img'])) echo 'checked' ?> value="1">Imagen de la factura </label> </td>
                        <td> <label><input type="checkbox" name="filters[promotor]" <?php if (isset($this->session->userdata('full_request_report')['promotor'])) echo 'checked' ?> value="1">Promotor </label> </td>
                        <td> <label><input type="checkbox" name="filters[beneficts]" <?php if (isset($this->session->userdata('full_request_report')['beneficts'])) echo 'checked' ?> value="1">Beneficios</label> </td>
                    </tr>

                    <tr>

                        <td> <label><input type="checkbox" name="filters[used_codes]" <?php if (isset($this->session->userdata('full_request_report')['used_codes'])) echo 'checked' ?> value="1">Códigos otorgados</label> </td>
                        <td> <label><input type="checkbox" name="filters[code_lg]" <?php if (isset($this->session->userdata('full_request_report')['code_lg'])) echo 'checked' ?> value="1">Código de beneficio LG</label> </td>
                        <td> <label><input type="checkbox" name="filters[promotional_code_txt]" <?php if (isset($this->session->userdata('full_request_report')['promotional_code_txt'])) echo 'checked' ?> value="1">Campo opcional</label> </td>
                        <td> <label><input type="checkbox" name="filters[request_date]" <?php if (isset($this->session->userdata('full_request_report')['request_date'])) echo 'checked' ?> value="1">Fecha de solicitud</label> </td>
                        <td> <label><input type="checkbox" name="filters[purchase_date]" <?php if (isset($this->session->userdata('full_request_report')['purchase_date'])) echo 'checked' ?> value="1">Fecha de compra</label> </td>
                        <td> <label><input type="checkbox" name="filters[status]" <?php if (isset($this->session->userdata('full_request_report')['status'])) echo 'checked' ?> value="1">Estado de solicitud</label></td>
                    </tr>

                    <tr>
                        <td> <label><input type="checkbox" name="filters[purchase_amount]" <?php if (isset($this->session->userdata('full_request_report')['purchase_amount'])) echo 'checked' ?> value="1">Precio de tvs</label></td>
                    </tr>

                </table>
            </div>
        </div>

        <h4>Filtros</h4>
        <div class="row">
            <div class="col-md-6">
                <table class="table">
                    <tr>
                        <td>Beneficio:
                        <select multiple class="make-fancy-multiselect"  name="benefict_filter[]" >
                            <?php foreach( $beneficts as $b ): ?>
                                <option <?php if( in_array(  $b->id, (array)$this->session->userdata('benefict_filter') ) )  echo 'selected' ?> value="<?php echo $b->id ?>"><?php echo $b->title ?></option>
                            <?php endforeach; ?>
                        </select>
                        </td>
                        <td>Referencia:
                        <select  name="reference_filter[]" multiple class="make-fancy-multiselect" >
                            <?php foreach( $references as $r ): ?>
                                <option <?php if( in_array(  $r->id, (array)$this->session->userdata('reference_filter') ) )  echo 'selected' ?> value="<?php echo $r->id ?>"><?php echo $r->reference ?></option>
                            <?php endforeach; ?>
                        </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <button class="btn btn-success" type="submit" >Aplicar</button>
            </div>
        </div>
    </form>
</div>

<p>
    <div class="row">
        <div class="col-md-2">
        <a target="_BLANK" href="<?php echo base_url() ?>site/register?admin_mode" class="btn btn-primary">Agrega solicitud</a>
        </div>
    </div>
</p>

<div>

    <?php echo $output; ?>
</div>
