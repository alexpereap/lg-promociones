<fieldset ng-controller="prizesController" class="form-horizontal"

<?php if( isset( $angular_data ) ): ?>

  ng-init = '
    store_qty = <?php echo $angular_data->store_qty; ?>;
    store_blocks = <?php echo json_encode( $angular_data->store_blocks ) ?>;
    cityStoresSwitch = <?php echo json_encode( $angular_data->cityStoresSwitch ) ?>;
    cityStores = <?php echo json_encode( $angular_data->cityStores ) ?>;
    cityStoresEmptyValue = <?php echo json_encode( $angular_data->cityStoresEmptyValue ) ?>;
    selected_values = <?php echo json_encode($angular_data->selected_values) ?>;
  '
<?php endif; ?>
>
<!-- Select Basic -->
  <div ng-repeat="s in store_blocks  track by $index"  >
    <input type="hidden"  value="{{ s.id }}" >
    <div class="form-group">
      <label class="col-md-4 control-label" for="city">Ciudad</label>
      <div class="col-md-6">
        <select ng-model="city_id" ng-change="updateSellPoint(city_id, s.id )" id="city"  class="form-control">
          <option value="">Seleccione...</option>
          <?php foreach( $cities as $c ):?>
          <option
          <?php if( isset( $angular_data ) ): ?>
            ng-selected="selected_values[ s.id ].city_id == <?php echo $c->id ?>"
          <?php endif; ?>
            value="<?php echo $c->id ?>"><?php echo $c->name ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="cityStores">Cadena</label>
      <div class="col-md-6">
        <select required ng-disabled="cityStoresSwitch[ s.id ]" id="cityStores" name="storeId[]" class="form-control">
          <option value="">{{ cityStoresEmptyValue[s.id] }}</option>
          <option
          <?php if( isset( $angular_data ) ): ?>
            ng-selected="selected_values[ s.id ].store_id == cs.id"
          <?php endif; ?>
          value="{{ cs.id }}" ng-repeat="cs in cityStores[ s.id ]" >{{ cs.name }}</option>
        </select>
      </div>
    </div>
</div>
<br>

<div class="form-group">
  <label class="col-md-4 control-label" ></label>
  <div class="col-md-8">
    <button id="saveBtn" ng-click="addStoreBlock()" type="button" class="btn btn-success">agregar cadena</button>
  </div>
</div>


</fieldset>