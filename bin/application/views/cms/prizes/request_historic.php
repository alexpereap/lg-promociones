<p>
    <a href="cms/full_request_report" class="btn btn-primary">Volver</a>
</p>
<div>
    <?php if( count($historic) == 0 ): ?>
        <p class="text-center" >Esta solicitud no cuenta con historial registrado.</p>
    <?php else: ?>
        <table class="table table-bordered table-striped table-responsive">
            <thead>
                <tr>
                    <th>Estado</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach( $historic as $h ): ?>
                <tr>
                    <td class="col-md-10" >
                        <?php switch( $h->status ){
                            case 'email_confirmacion':
                                echo "La solicitud ha entrado en estado de confirmación de email.";
                                break;

                            case 'en_revision':
                                echo "La solicitud ha entrado en estado de revisión.";
                                break;

                            case 'no_aprobado':
                                echo "El moderador ha rechazado la solicitud: <i>" . $h->detail . "</i>" ;
                                break;

                            case 'aprobado':
                                echo "El moderador ha aprobado la solicitud.";
                                break;

                            case 'entregado':
                                echo "Los beneficios se han entregado.";
                                break;

                        } ?>
                    </td class="col-md-2" >
                    <td><?php echo $h->date ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>

</div>