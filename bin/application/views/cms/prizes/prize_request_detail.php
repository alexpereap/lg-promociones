<form class="form-horizontal" method="POST" action="cms/set_prize_request_status" id="prizeDetailForm" >
<input type="hidden" name="prize_application_id" value="<?php echo $prize_detail[0]->prize_application_id ?>" >
<fieldset>

<!-- Form Name -->
<legend>Detalle solicitud</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" >Nombre completo:</label>
  <div class="col-md-4">
    <input readonly value="<?php echo $prize_detail[0]->name . ' ' . $prize_detail[0]->lastname ?>" class="form-control input-md">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" >Número de cédula:</label>
  <div class="col-md-4">
    <input readonly value="<?php echo $prize_detail[0]->nid ?>" class="form-control input-md">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" >Factura:</label>
  <div class="col-md-4">
    <img src="uploads/bill_imgs/<?php echo $prize_detail[0]->bill_img ?>"  width="100%" >
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" >Fecha de compra:</label>
  <div class="col-md-4">
    <input readonly value="<?php echo $prize_detail[0]->purchase_date ?>" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" >Fecha de solicitud:</label>
  <div class="col-md-4">
    <input readonly value="<?php echo $prize_detail[0]->request_date ?>" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" >Paquetes de promociones:</label>
  <div class="col-md-4">

      <ul id="reqDetalPrizeList">
      <?php
        $show_promo_code = false;
        foreach( $prize_detail as $p ):

        if( $p->prize_has_promo_code == 'yes' )
                $show_promo_code = true;
      ?>
        <li> <a target="_BLANK" href="cms/edit_prize_package/<?php echo $p->prize_id ?>" target="_BLANK" ><?php echo $p->prize_title ?></a> </li>
      <?php endforeach; ?>
      </ul>
  </div>
</div>

<?php if( $show_promo_code === true ): ?>
  <div class="form-group">
  <label class="col-md-4 control-label" >Código promocional:</label>
  <div class="col-md-4">
    <input type="text" readonly class="form-control input-md" value="<?php echo $promotional_code_txt ?>" >
  </div>
</div>
<?php endif; ?>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="request_status">Estado de la solicitud</label>
  <div class="col-md-4">
    <select id="request_status" name="request_status" class="form-control">
    <option <?php if( $prize_detail[0]->prize_status == 'email_confirmacion' ) echo 'selected' ?> value="email_confirmacion">Email de confirmación</option>
      <option <?php if( $prize_detail[0]->prize_status == 'en_revision' ) echo 'selected' ?> value="en_revision">En revisión</option>
      <option <?php if( $prize_detail[0]->prize_status == 'no_aprobado' ) echo 'selected' ?> value="no_aprobado">No aprobado</option>
      <option <?php if( $prize_detail[0]->prize_status == 'aprobado' ) echo 'selected' ?> value="aprobado">Aprobado</option>
      <option <?php if( $prize_detail[0]->prize_status == 'entregado' ) echo 'selected' ?> value="entregado">Entregado</option>
    </select>
  </div>
</div>

<div class="form-group" id="reqNotimailWrapper" <?php if( $prize_detail[0]->prize_status == 'aprobado' ): ?> style="display:block;"  <?php endif; ?>  >
  <label class="col-md-4 control-label" for="checkboxes">&nbsp;</label>
  <div class="col-md-4">
    <label class="checkbox-inline" for="sendApprovalMail">
      <input <?php if( $prize_detail[0]->prize_status == 'aprobado' )  echo "checked" ?> type="checkbox" name="sendApprovalMail" id="sendApprovalMail" value="1"  >
      Envíar email al cliente notificandole la aprobación
    </label>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="saveBtn"></label>
  <div class="col-md-8">
    <button id="saveBtn" ng-disabled="saveDisabled" name="saveBtn" type="submit" class="btn btn-success">Cambiar estado de la solicitud</button>
    <button id="cancelBtn" type="button" route="<?php echo base_url() ?>cms/prize_requests" name="cancelBtn" class="btn btn-primary">Volver al listado</button>
  </div>
</div>



</fieldset>
</form>
