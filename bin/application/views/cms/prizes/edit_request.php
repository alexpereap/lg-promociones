<link rel="stylesheet" href="vendor/jquery-ui-1.11.1.custom/jquery-ui.css" />
<script src="vendor/jquery-ui-1.11.1.custom/jquery-ui.min.js" ></script>

<form class="form-horizontal" method="POST" action="cms/update_purchase_request" enctype="multipart/form-data"  >
<input type="hidden" name="prize_application_id" value="<?php echo $request->prize_application_id ?>" >
<input type="hidden" name="user_id" value="<?php echo $request->user_id ?>" >
<input type="hidden" name="purchase_data_id" value="<?php echo $request->purchase_data_id ?>" >
<fieldset>

<!-- Form Name -->
<legend>Editar Solicitud</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Nombre</label>
  <div class="col-md-5">
  <input id="name" name="name" type="text" value="<?php echo $request->name ?>" placeholder="Nombre" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="lastname">Apellido</label>
  <div class="col-md-5">
  <input id="lastname" name="lastname" type="text" value="<?php echo $request->lastname ?>" placeholder="Apellido" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>
  <div class="col-md-5">
  <input id="email" name="email" type="text" value="<?php echo $request->email ?>" placeholder="Email" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cellphone">Celular</label>
  <div class="col-md-5">
  <input id="cellphone" name="cellphone" type="text" value="<?php echo $request->cellphone ?>" placeholder="Celular" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="phone_area_code">Código de área</label>
  <div class="col-md-1">
  <input id="phone_area_code" name="phone_area_code" value="<?php echo $request->phone_area_code ?>" type="text" placeholder="Código de área" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="phone">Teléfono</label>
  <div class="col-md-5">
  <input id="phone" name="phone" value="<?php echo $request->phone_only ?>" type="text" placeholder="Teléfono" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nid">Cédula</label>
  <div class="col-md-5">
  <input id="nid" name="nid" type="text" value="<?php echo $request->nid ?>" placeholder="Cédula" class="form-control input-md" required="">

  </div>
</div>

<!-- File Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="nid_img_front">Foto Cédula frontal</label>
  <div class="col-md-4">
    <input id="nid_img_front" name="nid_img_front" class="input-file" type="file">
    <div><a target="_BLANK" href="uploads/nid_imgs/<?php echo $request->nid_img_front ?>">ver imagen actual</a></div>
  </div>
</div>

<!-- File Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="nid_img_back">Foto Cédula trasera</label>
  <div class="col-md-4">
    <input id="nid_img_back" name="nid_img_back" class="input-file" type="file">
    <div><a target="_BLANK" href="uploads/nid_imgs/<?php echo $request->nid_img_back ?>">ver imagen actual</a></div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="promotor">Promotor</label>
  <div class="col-md-5">
  <input id="promotor" value="<?php echo $request->promotor ?>" name="promotor" type="text" placeholder="Promotor" class="form-control input-md" >
  <input type="hidden" name="promotor_id" id="promotor_id" value="<?php echo $request->promotor_id ?>" >
  </div>
</div>

<!-- File Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="bill_img">Foto de la factura</label>
  <div class="col-md-4">
    <input id="bill_img" name="bill_img" class="input-file" type="file">
    <div><a target="_BLANK" href="uploads/bill_imgs/<?php echo $request->bill_img ?>">ver imagen actual</a></div>
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="purchaseAmount">Precio de tvs</label>
  <div class="col-md-5">
    <div class="input-group">
      <span class="input-group-addon" >$</span>
      <input id="purchaseAmount" value="<?php echo $request->purchase_amount ?>" name="purchase_amount" type="text" placeholder="Precio de tvs" class="form-control input-md" >
    </div>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="saveBtn"></label>
  <div class="col-md-8">
    <button id="saveBtn" ng-disabled="saveDisabled" name="saveBtn" type="submit" class="btn btn-success">Guardar</button>
    <button  type="button" route="<?php echo base_url() ?>cms/full_request_report" name="cancelBtn" class="btn cancel-btn btn-danger">Cancelar</button>
  </div>
</div>

</fieldset>
</form>

<form class="form-horizontal" method="POST" action="cms/handle_request_status"  >
<input type="hidden" name="prize_application_id" value="<?php echo $request->prize_application_id ?>" >
<input type="hidden" name="user_id" value="<?php echo $request->user_id ?>" >
<input type="hidden" name="purchase_data_id" value="<?php echo $request->purchase_data_id ?>" >
<fieldset>
<legend>Cambiar estado de la solicitud</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="request_status">Estado de la solicitud</label>
  <div class="col-md-4">
    <select id="request_status" name="request_status" class="form-control">
    <option <?php if( $prize_detail[0]->prize_status == 'email_confirmacion' ) echo 'selected' ?> value="email_confirmacion">Email de confirmación</option>
      <option <?php if( $prize_detail[0]->prize_status == 'en_revision' ) echo 'selected' ?> value="en_revision">En revisión</option>
      <option <?php if( $prize_detail[0]->prize_status == 'no_aprobado' ) echo 'selected' ?> value="no_aprobado">No aprobado</option>
      <option <?php if( $prize_detail[0]->prize_status == 'aprobado' ) echo 'selected' ?> value="aprobado">Aprobado</option>
      <option <?php if( $prize_detail[0]->prize_status == 'entregado' ) echo 'selected' ?> value="entregado">Entregado</option>
    </select>
  </div>
</div>

<div class="form-group" id="reqNotimailWrapper" <?php if( $prize_detail[0]->prize_status == 'aprobado' ): ?> style="display:block;"  <?php endif; ?>  >
  <label class="col-md-4 control-label" for="checkboxes">&nbsp;</label>
  <div class="col-md-4">
    <label class="checkbox-inline" for="sendApprovalMail">
      <input <?php if( $prize_detail[0]->prize_status == 'aprobado' )  echo "checked" ?> type="checkbox" name="sendApprovalMail" id="sendApprovalMail" value="1"  >
      Envíar email al cliente notificandole la aprobación (se enviará una copia al moderador).
    </label>
  </div>
</div>

<div class="form-group" id="reqNotimailWrapper2" <?php if( $prize_detail[0]->prize_status == 'no_aprobado' ): ?> style="display:block;"  <?php endif; ?>  >
  <label class="col-md-4 control-label" for="checkboxes">&nbsp;</label>
  <div class="col-md-4">
    <label class="checkbox-inline" for="sendDenialMail">
      <input <?php if( $prize_detail[0]->prize_status == 'no_aprobado' )  echo "checked" ?> type="checkbox" name="sendDenialMail" id="sendDenialMail" value="1"  >
      Envíar email al cliente notificandole el rechazo de la soliciud.
    </label>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="saveBtn"></label>
  <div class="col-md-8">
    <button id="saveBtn" ng-disabled="saveDisabled" name="saveBtn" type="submit" class="btn btn-success">Cambiar estado de la solicitud</button>
    <button type="button" route="<?php echo base_url() ?>cms/full_request_report" name="cancelBtn" class="btn cancel-btn btn-primary">Volver al listado</button>
  </div>
</div>

</fieldset>
</form>