<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LG Electronics Colombia</title>
</head>

<body>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Tahoma, Geneva, sans-serif; color:#666666; padding:0 10px;">
  <tr>
    <td style="border-bottom: 1px solid #CCC; padding:20px 0"><table width="580" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="381" align="left" valign="middle"><img src="<?php echo base_url() ?>img/email/lg-logo.png" width="112" height="54" style="margin-left:20px; display:block; border:0;"></td>
          <td width="100" align="right" valign="bottom"><img src="<?php echo base_url() ?>img/email/siguenosen.gif" width="100" height="33"></td>
          <td width="33" align="center" valign="bottom"><a href="https://www.facebook.com/LGColombia" target="_blank"><img src="<?php echo base_url() ?>img/email/social-facebook.gif" width="33" height="33" alt="Facebook" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="https://twitter.com/soylg" target="_blank"><img src="<?php echo base_url() ?>img/email/social-twitter.gif" alt="Twitter" width="33" height="33" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="#" target="_blank"><img src="<?php echo base_url() ?>img/email/social-youtube.gif" alt="Youtube" width="33" height="33" style="display:block; border:0;"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td style="padding:20px 0"><p style="color:#c32357; font-size:20px; margin:5px 0;">&iexcl;Estás a un paso de recibir tus beneficios!</p>
      <p style="font-size:14px;  margin:5px 0">Verifica si tus datos están correctos y confírmalos.</p>
      <img src="<?php echo base_url() ?>img/email/datos-personales.gif" width="580" height="60" style="display:block; border:0; margin-top:20px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:3px solid #cccccc; background-color:#eeeeee; padding:20px 10px">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30%" style="font-size:12px; padding:3px 0;"><b>Nombre:</b></td>
                <td width="70%" style="font-size:12px;"><?php echo $application_info[0]->name ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Apellido:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->lastname ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Cédula:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->nid ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>E-mail:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->email ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Teléfono Celular:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->cellphone ?></td>
              </tr>

              <?php if( !empty( $application_info[0]->phone ) ): ?>
                <tr>
                  <td style="font-size:12px; padding:3px 0;"><b>Teléfono Residencia:</b></td>
                  <td style="font-size:12px;">
                  <?php if( !empty( $application_info[0]->phone_area_code ) ) echo '(+' . $application_info[0]->phone_area_code . ') ';  ?>
                  <?php echo $application_info[0]->phone ?>
                  </td>
                </tr>
              <?php endif; ?>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%" style="font-size:12px; padding:3px 0;"><b>Foto Frontal Cédula:</b></td>
                <td width="50%" style="font-size:12px; padding:3px 0;"><b>Foto Trasera Cédula:</b></td>
              </tr>
              <tr>
                <td width="50%" align="center" valign="top"><a target="_BLANK" href="<?php echo base_url() ?>uploads/nid_imgs/<?php echo $application_info[0]->nid_img_front ?>"><img src="<?php echo base_url() ?>timthumb.php?src=<?php echo base_url() ?>uploads/nid_imgs/<?php echo $application_info[0]->nid_img_front ?>&w=280&h=210&zc=2" width="280" height="210" style="border:0;"></a></td>
                <td width="50%" align="center" valign="top"><a target="_BLANK" href="<?php echo base_url() ?>uploads/nid_imgs/<?php echo $application_info[0]->nid_img_back ?>"><img src="<?php echo base_url() ?>timthumb.php?src=<?php echo base_url() ?>uploads/nid_imgs/<?php echo $application_info[0]->nid_img_back ?>&w=280&h=210&zc=2" width="280" height="210" style="border:0;"></a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <img src="<?php echo base_url() ?>img/email/datos-tv.gif" width="580" height="60" style="display:block; border:0; margin-top:20px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:3px solid #cccccc; background-color:#eeeeee; padding:20px 10px">
        <tr>
          <td>

            <?php foreach( $application_info  as $ai ): ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:10px 0;">
              <tr>
                <td width="30%" style="font-size:12px; padding:3px 0;"><b>Referencia:</b></td>
                <td width="70%" style="font-size:12px;"><?php echo $ai->tv_reference ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Serial:</b></td>
                <td style="font-size:12px;"><?php echo $ai->tv_serial ?></td>
              </tr>
            </table>
          <?php endforeach; ?>

            </td>
        </tr>
      </table>
      <img src="<?php echo base_url() ?>img/email/datos-compra.gif" width="580" height="60" style="display:block; border:0; margin-top:20px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:3px solid #cccccc; background-color:#eeeeee; padding:20px 10px">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30%" style="font-size:12px; padding:3px 0;"><b>Nombre:</b></td>
                <td width="70%" style="font-size:12px;"><?php echo $application_info[0]->name ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Apellido:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->lastname ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Cédula:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->nid ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>E-mail:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->email ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Teléfono Celular:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->cellphone ?></td>
              </tr>

              <?php if( !empty( $application_info[0]->phone ) ): ?>
                <tr>
                  <td style="font-size:12px; padding:3px 0;"><b>Teléfono Residencia:</b></td>
                  <td style="font-size:12px;">
                  <?php if( !empty( $application_info[0]->phone_area_code ) ) echo '(+' . $application_info[0]->phone_area_code . ') ';  ?>
                  <?php echo $application_info[0]->phone ?>
                  </td>
                </tr>
              <?php endif; ?>

            <?php if(  !empty($application_info[0]->promotor_id) ): ?>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Promotor:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->promotor_name . ' ' . $application_info[0]->promotor_lastname ?></td>
              </tr>
            <?php endif; ?>

            <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Ciudad:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->city_name ?></td>
              </tr>

            <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Cadena:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->store_name ?></td>
              </tr>

              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Punto de venta:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->sellpoint_name ?></td>
              </tr>

              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Fecha de compra:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->purchase_date ?></td>
              </tr>

              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Precio de tvs:</b></td>
                <td style="font-size:12px;"><?php echo "$" . $application_info[0]->purchase_amount ?></td>
              </tr>

            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Foto Facutra:</b></td>
              </tr>
              <tr>
                <td align="center" valign="top"><a target="_BLANK" href="<?php echo base_url() ?>uploads/bill_imgs/<?php echo $application_info[0]->bill_img ?>"><img src="<?php echo base_url() ?>timthumb.php?src=<?php echo base_url() ?>uploads/bill_imgs/<?php echo $application_info[0]->bill_img ?>&w=400&h=266&zc=2" width="400" height="266" style="border:0;"></a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:15px;">
        <tr>
          <td><a target="_BLANK" href="<?php echo base_url() ?>site/register_update?token=<?php echo $encrypted_app_id ?>"><img src="<?php echo base_url() ?>img/email/btEdit.gif" width="290" height="70" style="border:0;"></a></td>
          <td><a target="_BLANK" href="<?php echo base_url() ?>site/confirm_data?token=<?php echo $encrypted_app_id ?>"><img src="<?php echo base_url() ?>img/email/btConfirm.gif" width="290" height="70" style="border:0;"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td style="border-top: 1px solid #CCC; padding:20px 0; font-size:14px">Línea de atención 018000 910 683<br/>
      <a href="http://www.lg.com/co" target="_blank" style="color:#666666; text-decoration:none;">www.lg.com/co</a><br/>
      <span style="color:#c32357">Con LG todo es posible.</span><br/>
      <br/>
      <span style="font-size:11px;">Copyright © 2014 LG Electronics. Todos los derechos reservados.</span></td>
  </tr>
</table>
</body>
</html>
