<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LG Electronics Colombia</title>
</head>

<body>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Tahoma, Geneva, sans-serif; color:#666666; padding:0 10px;">
  <tr>
    <td style="border-bottom: 1px solid #CCC; padding:20px 0"><table width="580" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="381" align="left" valign="middle"><img src="<?php echo base_url() ?>img/email/lg-logo.png" width="112" height="54" style="margin-left:20px; display:block; border:0;"></td>
          <td width="100" align="right" valign="bottom"><img src="<?php echo base_url() ?>img/email/siguenosen.gif" width="100" height="33"></td>
          <td width="33" align="center" valign="bottom"><a href="https://www.facebook.com/LGColombia" target="_blank"><img src="<?php echo base_url() ?>img/email/social-facebook.gif" width="33" height="33" alt="Facebook" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="https://twitter.com/soylg" target="_blank"><img src="<?php echo base_url() ?>img/email/social-twitter.gif" alt="Twitter" width="33" height="33" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="#" target="_blank"><img src="<?php echo base_url() ?>img/email/social-youtube.gif" alt="Youtube" width="33" height="33" style="display:block; border:0;"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td style="padding:20px 0"><p style="color:#c32357; font-size:20px; margin:5px 0;">¡FELICITACIONES! Tu registro fue satisfactorio.</p>
      <p style="font-size:14px;  margin:5px 0">Ahora puedes acceder a los siguientes beneficios.</p>
      <?php foreach( $prizes_view as $pv ): ?>
      <table width="580" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #cccccc; margin:20px 0; padding-top:20px">
        <tr>
          <td width="45%" align="center" valign="middle"><img src="<?php echo base_url() ?>timthumb.php?src=<?php echo base_url() ?>assets/uploads/files/<?php echo $pv->prize_detail_image ?>&w=200&h=200&zc=2" width="200" height="200" style="display:block; border:0;"></td>
          <td width="55%" align="left" valign="middle"><p style="color:#c32357; font-size:18px; margin:5px 0;"><?php echo $pv->prize_detail_title ?></p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:3px solid #cccccc; background-color:#eeeeee; padding:20px 10px 10px">
              <tr>
                <td><p style="font-size:13px; margin:0;"><?php echo $pv->prize_detail_reclame_description ?></p>

                  <?php if( $pv->prize_detail_has_list_of_codes == 'yes' ): ?>
                  <span style="background-color:#ffffff; color:#333333; width:100%; text-align:center; font-size:18px; display:block; margin-top:20px; padding:5px 0;"><?php echo $pv->code ?></span></td>
                <?php endif; ?>
              </tr>
            </table></td>
        </tr>
      </table>
      <?php endforeach; ?>
      <table width="580" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #cccccc; margin:20px 0; padding-top:20px">
        <tr>
          <td><p style="color:#c32357; font-size:18px; margin:5px 0;">NUESTRAS OFICINAS:</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="20%" style="font-size:12px;">Bogotá<br/>
                  Medellín<br/>
                  Cali<br/>
                  Barranquilla<br/>
                  Bucaramanga<br/>
                  Pereira</td>
                <td width="80%" style="font-size:12px;">&rsaquo; Cra 11 #94 A – 34<br/>
                  &rsaquo; Calle 33 No. 66 B – 45 Bulerias-Laureles<br/>
                  &rsaquo; Av. Américas # 18 N – 26 Of. 2501 Ed. Torre de Cali<br/>
                  &rsaquo; Carrera 46 # 80 - 287<br/>
                  &rsaquo; Calle 48 # 32 – 67 Piso 2<br/>
                  &rsaquo; Cra 15 # 10B- 01 Local 1 Barrio Los Alpes</td>
              </tr>
            </table>
            <p style="font-size:13px; margin:20px 0 0;"><b>Horario:</b><br/>
              Lunes a Miércoles de 8:00am – 12:00m y 1:00pm – 5:00pm</p></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td style="border-top: 1px solid #CCC; padding:20px 0; font-size:14px">Línea de atención 018000 910 683<br/>
      <a href="http://www.lg.com/co" target="_blank" style="color:#666666; text-decoration:none;">www.lg.com/co</a><br/>
      <span style="color:#c32357">Con LG todo es posible.</span><br/>
      <br/>
      <span style="font-size:11px;">Copyright © 2014 LG Electronics. Todos los derechos reservados.</span></td>
  </tr>
</table>
</body>
</html>
