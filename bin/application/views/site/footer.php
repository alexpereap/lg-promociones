 <!-- FOOTER-->
    <footer class="brdrTop">
       <div class="row">
            <div class="col-sm-6">
                <h4>Línea de atención 018000 910 683</h4>
                <a onclick="ga('send','event', 'Footer','Click','lg.com/co_Footer');" href="http://www.lg.com/co" target="_blank">www.lg.com/co</a>
                <h4 class="colorRed">Con LG todo es posible</h4>
            </div>
            <div class="col-sm-6 text-right"> <a onclick="ga('send','event', 'Footer','Click','Términos-y-Condiciones_Footer');" href="#modalTerms" data-toggle="modal" class="linkTerms trnstn">Términos y Condiciones</a></div>
       </div>
       <div class="row">
            <div class="col-sm-12">
            <br>
                <p class="small">Copyright © 2014 LG Electronics. Todos los derechos reservados</p>
            </div>
       </div>
    </footer>
    <!-- FOOTER END-->
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54883719-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>